FROM ubuntu

RUN apt-get update -y;apt-get install -y python3-pip python3-dev;apt-get autoclean

WORKDIR /root

COPY execute_test.sh requirements.txt /root/
COPY broker /root/broker

RUN mkdir /config;pip3 install --no-cache-dir -r requirements.txt

ENV KUBECONFIG=/config/kube.config


EXPOSE 8080/tcp
ENTRYPOINT ["/root/execute_test.sh"]
CMD []
