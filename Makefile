#
USER_DIR=broker
OUTPUT_DIR=swagger-output
#PLATAFORMS=linux/arm64
PLATAFORMS=linux/amd64,linux/arm64
#PLATAFORMS=linux/amd64,linux/arm64,linux/arm
DATE_RUN := $(shell date +"%Y%m%d%H%M")

${OUTPUT_DIR}/README.md: swagger.json
	-rm -rf ${OUTPUT_DIR}.old
	-mv ${OUTPUT_DIR} ${OUTPUT_DIR}.old
	swagger-codegen generate -i $< -l python-flask -o ${OUTPUT_DIR}
	-diff -Nru -x "*.pyc" ${OUTPUT_DIR}.old ${OUTPUT_DIR} > patch-swagger.old-new.${DATE_RUN}
	-diff -Nru -x "*.pyc" ${OUTPUT_DIR} ${USER_DIR} > patch-swagger.patch.${DATE_RUN}

patch:
	-diff -Nru -x "*.pyc" ${OUTPUT_DIR}.old ${OUTPUT_DIR} > patch-swagger.old-new.${DATE_RUN}
	-diff -Nru -x "*.pyc" ${OUTPUT_DIR} ${USER_DIR} > patch-swagger.patch.${DATE_RUN}

image-broker:
	#docker buildx build --output=type=image,name=registry.gitlab.com/arm-research/smarter/smarter-brokerage-server:${DATE_RUN},push=true --platform ${PLATAFORMS} .
	docker buildx build --push --platform ${PLATAFORMS} -t registry.gitlab.com/arm-research/smarter/smarter-brokerage/server:${DATE_RUN} .

image-dind:
	cd dind/image;docker buildx build --push --platform ${PLATAFORMS} -t registry.gitlab.com/arm-research/smarter/smarter-brokerage/dind:${DATE_RUN} .

clean:
	rm -rf ${OUTPUT_DIR}
