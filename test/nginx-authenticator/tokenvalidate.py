# tokenvalidate.py

import falcon
import jwt
import os

class TokenValidateResource:
    def on_get(self, req, resp):
        """decode and verify"""
        secret = os.environ["BROKERAGE_SECRET_KEY"]
        try:
            token = req.auth[7:]
            print('Token received:',req.auth,': and the cut one:',req.auth[7:],':')
            payload = jwt.decode(token, secret, algorithms=['HS256'])
            print('Payload:',payload)
    
            resp.media = ""
            resp.append_header('X-AccountRole', payload['role'])
            resp.append_header('X-AccountID', payload['accountID'])
        except:
            resp.status = falcon.HTTP_401  # pylint: disable=no-member

api = falcon.API()
api.add_route('/token/validate', TokenValidateResource())
