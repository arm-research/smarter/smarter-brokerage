#!/bin/sh

exec gunicorn tokenvalidate:api \
     --bind 0.0.0.0:8000 \
     --workers ${CHIMERA_WS_WORKERS:-"3"} \
     --log-level=${CHIMERA_WS_LOGLEVEL:-"debug"}
