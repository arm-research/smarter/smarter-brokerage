# tokenvalidate.py

import jwt
import os
import sys

if len(sys.argv) > 2 or not "BROKERAGE_SECRET_KEY" in os.environ:
   print('Usage:',sys.argv[0],'[<accountID>]')
   print('The role will be broker it the accountID is not provided otherwise the role is tenant')
   print('The secret should be on the environment variable BROKERAGE_SECRET_KEY')
   sys.exit(1)

if len(sys.argv) < 2:
   print('Creating a jwt token for role: broker')
   tokenDict={'role':'broker','accountID':''}
else:
   tokenDict={'role':'tenant','accountID':sys.argv[1]}
   print('Creating a jwt token for role: tenant accountID:',sys.argv[1])
 
payload = jwt.encode(tokenDict, os.environ["BROKERAGE_SECRET_KEY"], algorithm='HS256')


payloadStr=str(payload,'utf-8')

payloadDecoded = jwt.decode(payloadStr, os.environ["BROKERAGE_SECRET_KEY"], algorithms=['HS256'])

print('token decoded:',payloadDecoded)
print('token:',payloadStr)
