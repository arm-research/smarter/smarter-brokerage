#!/bin/bash

STOP_WAIT_AFTER_WGET=true
DIRCMD=$(dirname $0)

if [ -e ${DIRCMD}/test-tenant-creat-vars.sh ]
then
        . ${DIRCMD}/test-tenant-creat-vars.sh
fi

##### Do not forget to set the variables below

#WGET_HOST      URI of the brokerage for example "http://127.0.0.1:8080"
#TENANTID_USED  ID of the tenant to be used
#VNODE_USED     Name of the virtual node to be created
#PNODE_USED     Name of the physical node to be used
#TENANT_NODE_MANAGER_CONNECTION_INFO    IP and port of the k3s node manager ex: https://192.168.1.1:6443
#TENANT_NODE_MANAGER_TOKEN   token from the k3s node manager that allows nodes to register
#TENANT_NODE_MANAGER_KUBECONFIG_FILE Credentials to access the node manager from kubectl

if [ -z "${WGET_HOST}" -o -z "${TENANTID_USED}" -o -z "${VNODE_USED}" -o -z "${PNODE_USED}" -o -z "${TENANT_NODE_MANAGER_CONNECTION_INFO}" -o -z "${TENANT_NODE_MANAGER_TOKEN}" -o -z "${TENANT_NODE_MANAGER_KUBECONFIG_FILE}" ]
then
        echo 'Configuration variables not set'
        exit -1
fi

if [ ! -e "${TENANT_NODE_MANAGER_KUBECONFIG_FILE}" ]
then
        echo "Could not find the file ${TENANT_NODE_MANAGER_KUBECONFIG_FILE} that allows kubectl to work"
        exit -1
fi

export KUBECONFIG="${TENANT_NODE_MANAGER_KUBECONFIG_FILE}"

echo "Checking if kubectl can access the tenant node manager, by executing kubectl version"
echo '--------------------------------------------------------'
kubectl version
echo '--------------------------------------------------------'

echo "Adding the SMARTER CNI and DNS to the tenant Node Manager" 
echo '--------------------------------------------------------'
kubectl apply -f ${DIRCMD}/smartercni_ds-vnode.yaml
kubectl apply -f ${DIRCMD}/smarterdns_ds_docker-vnode.yaml
echo '--------------------------------------------------------'

ERROROUT="$(mktemp)"

function wgetsh() {
        METHOD=$1
        ROLE=$2
        ID=$3
        URL=$4
        POSTFILE=$5

        echo '--------------------------------------------------------'
        echo "wgetsh ${METHOD} ${ROLE} ${ID} ${URL} ${POSTFILE}"
        if [ ! -z "${POSTFILE}" ]
        then
                echo 'Json object to add' 
                cat ${POSTFILE}
        fi
        echo '--------------------------------------------------------'
        if [ "${STOP_WAIT_AFTER_WGET}" = "true" ]
        then
                echo 'Press a key to execute the query, or ctrl-c to stop'
                read
        fi

        WGET_ROLE=
        WGET_ID=
        WGET_METHOD=
        WGET_POSTFILE=
        WGET_POSTHEADER=

        if [ ! -z "${ROLE}" ]; then
                WGET_ROLE="--header=X-AccountRole: ${ROLE}"
        fi
        if [ ! -z "${ID}" ]; then
                WGET_ID="--header=X-AccountID: ${ID}"
        fi

        if [ "${METHOD}" != "GET" ]; then
                WGET_METHOD="--method=${METHOD}"
                if [ "${METHOD}" == "POST" ]; then
                        WGET_POSTFILE="--body-file=${POSTFILE}"
                        WGET_POSTHEADER="--header=Content-Type:application/json"
                fi
        fi

        OUTPUT=$(wget ${WGET_OPTIONS} "${WGET_ROLE}" "${WGET_ID}" "${WGET_METHOD}" "${WGET_POSTHEADER}" "${WGET_POSTFILE}" "${WGET_POSTFILE}" "${WGET_HOST}${URL}" 2>${ERROROUT})
        ERROR=$(cat ${ERROROUT})
        if [ ! -z "${OUTPUT}" ]
        then
                HTTPRETCODE=$(echo ${OUTPUT} | tr "\r" "\n" | grep "^HTTP" | head -n 1 | cut -d " " -f 2)
                echo "${OUTPUT}"
        else
                echo "${ERROR}"
        fi
                
        echo '=========================================================' 

}

WGET_OPTIONS="-S --content-on-error  -q -O - --save-headers"

wgetsh GET   broker  ""      /api/v1alpha1/tenants

wgetsh GET   broker  ""      /api/v1alpha1/tenants/${TENANTID_USED}
if [ "${HTTPRETCODE}" == "200" ]
then
        wgetsh DELETE   broker  ""      /api/v1alpha1/tenants/${TENANTID_USED}
        if [ "${HTTPRETCODE}" != "200" ]
        then
                echo 'Error on executing the delete, please check the output'
                exit -1
        fi
        echo 'Waiting 10 seconds for all things to be deleted'

        while true
        do
                sleep 10
                wgetsh GET      broker  ""      /api/v1alpha1/tenants/${TENANTID_USED}
                if [ "${HTTPRETCODE}" == "404" ]
                then
                        break
                fi
                echo 'Tenant is still being deleted, Waiting 30 seconds for all things to be deleted'
        done
else
        # This is just to show executing DELETE on a non-existing object

        wgetsh DELETE   broker  ""      /api/v1alpha1/tenants/${TENANTID_USED}
        if [ "${HTTPRETCODE}" != "404" ]
        then
                echo 'Error on executing the delete, please check the output'
                exit -1
        fi
        echo "The tenant ${TENANTID_USED} does not exist so the DELETE should fail as it did (404)"
fi

cat <<EOF > template.json.tmp
{
     "id": "${TENANTID_USED}",
     "name": "${TENANTID_USED} name",
     "credential": {
          "server": "${TENANT_NODE_MANAGER_CONNECTION_INFO}",
          "token": "${TENANT_NODE_MANAGER_TOKEN}"
     }
}
EOF
wgetsh POST     broker  ""      /api/v1alpha1/tenants                                                   template.json.tmp
if [ "${HTTPRETCODE}" != "200" ]
then
        echo 'Error on executing the create, please check the output'
        exit -1
fi

wgetsh GET      broker  ""      /api/v1alpha1/tenants
if [ "${HTTPRETCODE}" != "200" ]
then
        echo 'Error on executing the get, please check the output'
        exit -1
fi

wgetsh GET      tenant  ${TENANTID_USED} /api/v1alpha1/tenants/${TENANTID_USED}
if [ "${HTTPRETCODE}" != "200" ]
then
        echo 'Error on executing the get, the tenant should exist since we just created the tenant, please check the output'
        exit -1
fi

wgetsh GET      broker  ""      /api/v1alpha1/physicalnodes
if [ "${HTTPRETCODE}" != "200" ]
then
        echo 'Error on executing the get, please check the output'
        exit -1
fi

wgetsh GET   broker  ""      /api/v1alpha1/physicalnodes/${PNODE_USED}
if [ "${HTTPRETCODE}" == "200" ]
then
        wgetsh DELETE   broker  ""      /api/v1alpha1/physicalnodes/${PNODE_USED}
        if [ "${HTTPRETCODE}" != "200" ]
        then
                echo 'Error on executing the delete, please check the output'
                exit -1
        fi
        echo 'Waiting 10 seconds for all things to be deleted'
else
        # This is just to show executing DELETE on a non-existing object

        wgetsh DELETE   broker  ""      /api/v1alpha1/physicalnodes/${PNODE_USED}
        if [ "${HTTPRETCODE}" != "404" ]
        then
                echo 'Error on executing the delete, please check the output'
                exit -1
        fi
        echo "The physicalNode ${PNODE_USED} does not exist so the DELETE should fail as it did (404)"
fi

wgetsh GET      broker  ""      /api/v1alpha1/physicalnodes/${PNODE_USED}
if [ "${HTTPRETCODE}" != "404" ]
then
        echo "The physicalNode ${PNODE_USED} was deleted or did not exist before so something is wrong, please check the output" 
        exit -1
fi

cat <<EOF > template.json.tmp
{
     "id": "${PNODE_USED}",
     "name": "node broker"
}
EOF
wgetsh POST     broker  ""      /api/v1alpha1/physicalnodes                                             template.json.tmp
if [ "${HTTPRETCODE}" != "200" ]
then
        echo 'Error on executing the create, please check the output'
        exit -1
fi

wgetsh GET      broker  ""      /api/v1alpha1/physicalnodes
if [ "${HTTPRETCODE}" != "200" ]
then
        echo 'Error on executing the list, please check the output'
        exit -1
fi

wgetsh GET      tenant  ${TENANTID_USED} /api/v1alpha1/tenants/${TENANTID_USED}/nodes
if [ "${HTTPRETCODE}" != "200" ]
then
        echo 'Error on executing the list, please check the output'
        exit -1
fi

wgetsh GET   tenant  ${TENANTID_USED} /api/v1alpha1/tenants/${TENANTID_USED}/nodes/${VNODE_USED}
if [ "${HTTPRETCODE}" == "200" ]
then
        wgetsh DELETE   tenant  ${TENANTID_USED} /api/v1alpha1/tenants/${TENANTID_USED}/nodes/${VNODE_USED}
        if [ "${HTTPRETCODE}" != "200" ]
        then
                echo 'Error on executing the delete, please check the output'
                exit -1
        fi
        echo 'Waiting 10 seconds for all things to be deleted'

        while true
        do
                sleep 10
                wgetsh GET      tenant  ${TENANTID_USED} /api/v1alpha1/tenants/${TENANTID_USED}/nodes/${VNODE_USED}
                if [ "${HTTPRETCODE}" == "404" ]
                then
                        break
                fi
                echo 'node is still being deleted, Waiting 30 seconds for all things to be deleted'
        done
else
        # This is just to show executing DELETE on a non-existing object

        wgetsh DELETE   tenant  ${TENANTID_USED} /api/v1alpha1/tenants/${TENANTID_USED}/nodes/${VNODE_USED}
        if [ "${HTTPRETCODE}" != "404" ]
        then
                echo 'Error on executing the delete, please check the output'
                exit -1
        fi
        echo "The node ${VNODE_USED} does not exist so the DELETE should fail as it did (404)"
fi

echo "Deleting the node on the tenant SMARTER Node Manager" 
kubectl delete node ${VNODE_USED}

cat << EOF > template.json.tmp
{
     "id": "${VNODE_USED}",
     "name": "node 1",
     "physicalNode": "${PNODE_USED}"
}
EOF
wgetsh POST     tenant  ${TENANTID_USED} /api/v1alpha1/tenants/${TENANTID_USED}/nodes                                     template.json.tmp
if [ "${HTTPRETCODE}" != "200" ]
then
        echo 'Error on executing the create, please check the output'
        exit -1
fi

echo 'Waiting 10 seconds for the node to be active (registed to the node manager, etc....)'
sleep 10

wgetsh GET      tenant  ${TENANTID_USED} /api/v1alpha1/tenants/${TENANTID_USED}/nodes/${VNODE_USED}
if [ "${HTTPRETCODE}" != "200" ]
then
        echo 'Error on executing the get the node should be available by now, please check the output'
        exit -1
fi

echo 'Waiting up to 240 seconds for the node to be active (registed to the node manager, etc....'

echo "Executing kubectl get nodes, the node should be there as NotReady, since CNI and DNS are not deployed yet"
i=48
while [ $i -gt 0 ]
do
        RESULT=$(kubectl get node ${VNODE_USED} 2>/dev/null)

        if [ ! -z "${RESULT}" ]
        then
                echo "${RESULT}"
                break
        fi
        echo "Node is not active yet, waiting 5 seconds and trying more $i times"
        sleep 5
        i=$(($i-1))
done

if [ $i -eq 0 ]
then
        echo "Node was not active in time...... bailing......"
        exit 1
fi

echo "Executing kubectl label node so CNI and DNS are deployed"
kubectl label node ${VNODE_USED} smarter.cni.vnode=deploy
kubectl label node ${VNODE_USED} smarter.cri.vnode=docker

echo "The following command does not end, type ctrl-c to end it, (executing kubectl get nodes -w), the node should change to Ready in a few minutes if the download speed of the containers is slow"
kubectl get nodes -w 

exit 0
