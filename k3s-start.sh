#!/bin/bash

# Identification of the server, used for directories names and disambiguate tokens and KUBECONFIG files
SERVERNAME=tenant1 
# Especific port to be used at the server
HOSTPORT=7443
# IP that the clients will be used to connect (If on the cloud it will probably be the external IP of the server)
HOSTIP=192.168.20.15o

# Which version of k3s or k8s to use
DOCKERIMAGE=rancher/k3s:v1.20.4-k3s1


LOCALSERVERDIR=$(pwd)/${SERVERNAME}

CONTAINERID=$(docker run -d --rm -p ${HOSTPORT}:${HOSTPORT} -v ${LOCALSERVERDIR}:/var/lib/rancher/k3s ${DOCKERIMAGE} server --tls-san ${HOSTIP} --advertise-address ${HOSTIP} --https-listen-port ${HOSTPORT} --disable-agent --no-deploy servicelb --no-deploy traefik --no-deploy metrics-server --no-flannel --no-deploy coredns)

if [ $? -gt 0 ]
then
        echo "Docker failed......"
        exit -1
fi

echo "Container ID: ${CONTAINERID}"

echo "waiting for the container to generate the secrets"

sleep 30 # have to wait for k3s to generate the file

echo "Copying the secrets, token to token.${SERVERNAME} and kube.confg to kube.${SERVERNAME}.config"
docker cp ${CONTAINERID}:/etc/rancher/k3s/k3s.yaml kube.${SERVERNAME}.config
docker cp ${CONTAINERID}://var/lib/rancher/k3s/server/token token.${SERVERNAME}
sed -i -e "s/\(https:\/\/\)127.0.0.1/\1${HOSTIP}/" kube.${SERVERNAME}.config
