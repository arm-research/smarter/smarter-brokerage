# coding: utf-8

from __future__ import absolute_import
from datetime import date, datetime  # noqa: F401

from typing import List, Dict  # noqa: F401

from swagger_server.models.base_model_ import Model
from swagger_server.models.io_k8s_apimachinery_pkg_apis_meta_v1_api_resource import IoK8sApimachineryPkgApisMetaV1APIResource  # noqa: F401,E501
from swagger_server import util


class IoK8sApimachineryPkgApisMetaV1APIResourceList(Model):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    def __init__(self, api_version: str=None, group_version: str=None, kind: str=None, resources: List[IoK8sApimachineryPkgApisMetaV1APIResource]=None):  # noqa: E501
        """IoK8sApimachineryPkgApisMetaV1APIResourceList - a model defined in Swagger

        :param api_version: The api_version of this IoK8sApimachineryPkgApisMetaV1APIResourceList.  # noqa: E501
        :type api_version: str
        :param group_version: The group_version of this IoK8sApimachineryPkgApisMetaV1APIResourceList.  # noqa: E501
        :type group_version: str
        :param kind: The kind of this IoK8sApimachineryPkgApisMetaV1APIResourceList.  # noqa: E501
        :type kind: str
        :param resources: The resources of this IoK8sApimachineryPkgApisMetaV1APIResourceList.  # noqa: E501
        :type resources: List[IoK8sApimachineryPkgApisMetaV1APIResource]
        """
        self.swagger_types = {
            'api_version': str,
            'group_version': str,
            'kind': str,
            'resources': List[IoK8sApimachineryPkgApisMetaV1APIResource]
        }

        self.attribute_map = {
            'api_version': 'apiVersion',
            'group_version': 'groupVersion',
            'kind': 'kind',
            'resources': 'resources'
        }
        self._api_version = api_version
        self._group_version = group_version
        self._kind = kind
        self._resources = resources

    @classmethod
    def from_dict(cls, dikt) -> 'IoK8sApimachineryPkgApisMetaV1APIResourceList':
        """Returns the dict as a model

        :param dikt: A dict.
        :type: dict
        :return: The io.k8s.apimachinery.pkg.apis.meta.v1.APIResourceList of this IoK8sApimachineryPkgApisMetaV1APIResourceList.  # noqa: E501
        :rtype: IoK8sApimachineryPkgApisMetaV1APIResourceList
        """
        return util.deserialize_model(dikt, cls)

    @property
    def api_version(self) -> str:
        """Gets the api_version of this IoK8sApimachineryPkgApisMetaV1APIResourceList.

        APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#resources  # noqa: E501

        :return: The api_version of this IoK8sApimachineryPkgApisMetaV1APIResourceList.
        :rtype: str
        """
        return self._api_version

    @api_version.setter
    def api_version(self, api_version: str):
        """Sets the api_version of this IoK8sApimachineryPkgApisMetaV1APIResourceList.

        APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#resources  # noqa: E501

        :param api_version: The api_version of this IoK8sApimachineryPkgApisMetaV1APIResourceList.
        :type api_version: str
        """

        self._api_version = api_version

    @property
    def group_version(self) -> str:
        """Gets the group_version of this IoK8sApimachineryPkgApisMetaV1APIResourceList.

        groupVersion is the group and version this APIResourceList is for.  # noqa: E501

        :return: The group_version of this IoK8sApimachineryPkgApisMetaV1APIResourceList.
        :rtype: str
        """
        return self._group_version

    @group_version.setter
    def group_version(self, group_version: str):
        """Sets the group_version of this IoK8sApimachineryPkgApisMetaV1APIResourceList.

        groupVersion is the group and version this APIResourceList is for.  # noqa: E501

        :param group_version: The group_version of this IoK8sApimachineryPkgApisMetaV1APIResourceList.
        :type group_version: str
        """
        if group_version is None:
            raise ValueError("Invalid value for `group_version`, must not be `None`")  # noqa: E501

        self._group_version = group_version

    @property
    def kind(self) -> str:
        """Gets the kind of this IoK8sApimachineryPkgApisMetaV1APIResourceList.

        Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#types-kinds  # noqa: E501

        :return: The kind of this IoK8sApimachineryPkgApisMetaV1APIResourceList.
        :rtype: str
        """
        return self._kind

    @kind.setter
    def kind(self, kind: str):
        """Sets the kind of this IoK8sApimachineryPkgApisMetaV1APIResourceList.

        Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#types-kinds  # noqa: E501

        :param kind: The kind of this IoK8sApimachineryPkgApisMetaV1APIResourceList.
        :type kind: str
        """

        self._kind = kind

    @property
    def resources(self) -> List[IoK8sApimachineryPkgApisMetaV1APIResource]:
        """Gets the resources of this IoK8sApimachineryPkgApisMetaV1APIResourceList.

        resources contains the name of the resources and if they are namespaced.  # noqa: E501

        :return: The resources of this IoK8sApimachineryPkgApisMetaV1APIResourceList.
        :rtype: List[IoK8sApimachineryPkgApisMetaV1APIResource]
        """
        return self._resources

    @resources.setter
    def resources(self, resources: List[IoK8sApimachineryPkgApisMetaV1APIResource]):
        """Sets the resources of this IoK8sApimachineryPkgApisMetaV1APIResourceList.

        resources contains the name of the resources and if they are namespaced.  # noqa: E501

        :param resources: The resources of this IoK8sApimachineryPkgApisMetaV1APIResourceList.
        :type resources: List[IoK8sApimachineryPkgApisMetaV1APIResource]
        """
        if resources is None:
            raise ValueError("Invalid value for `resources`, must not be `None`")  # noqa: E501

        self._resources = resources
