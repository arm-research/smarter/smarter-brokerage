# coding: utf-8

from __future__ import absolute_import
from datetime import date, datetime  # noqa: F401

from typing import List, Dict  # noqa: F401

from swagger_server.models.base_model_ import Model
from swagger_server.models.io_k8s_apimachinery_pkg_apis_meta_v1_server_address_by_client_cidr import IoK8sApimachineryPkgApisMetaV1ServerAddressByClientCIDR  # noqa: F401,E501
from swagger_server import util


class IoK8sApimachineryPkgApisMetaV1APIVersions(Model):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    def __init__(self, api_version: str=None, kind: str=None, server_address_by_client_cid_rs: List[IoK8sApimachineryPkgApisMetaV1ServerAddressByClientCIDR]=None, versions: List[str]=None):  # noqa: E501
        """IoK8sApimachineryPkgApisMetaV1APIVersions - a model defined in Swagger

        :param api_version: The api_version of this IoK8sApimachineryPkgApisMetaV1APIVersions.  # noqa: E501
        :type api_version: str
        :param kind: The kind of this IoK8sApimachineryPkgApisMetaV1APIVersions.  # noqa: E501
        :type kind: str
        :param server_address_by_client_cid_rs: The server_address_by_client_cid_rs of this IoK8sApimachineryPkgApisMetaV1APIVersions.  # noqa: E501
        :type server_address_by_client_cid_rs: List[IoK8sApimachineryPkgApisMetaV1ServerAddressByClientCIDR]
        :param versions: The versions of this IoK8sApimachineryPkgApisMetaV1APIVersions.  # noqa: E501
        :type versions: List[str]
        """
        self.swagger_types = {
            'api_version': str,
            'kind': str,
            'server_address_by_client_cid_rs': List[IoK8sApimachineryPkgApisMetaV1ServerAddressByClientCIDR],
            'versions': List[str]
        }

        self.attribute_map = {
            'api_version': 'apiVersion',
            'kind': 'kind',
            'server_address_by_client_cid_rs': 'serverAddressByClientCIDRs',
            'versions': 'versions'
        }
        self._api_version = api_version
        self._kind = kind
        self._server_address_by_client_cid_rs = server_address_by_client_cid_rs
        self._versions = versions

    @classmethod
    def from_dict(cls, dikt) -> 'IoK8sApimachineryPkgApisMetaV1APIVersions':
        """Returns the dict as a model

        :param dikt: A dict.
        :type: dict
        :return: The io.k8s.apimachinery.pkg.apis.meta.v1.APIVersions of this IoK8sApimachineryPkgApisMetaV1APIVersions.  # noqa: E501
        :rtype: IoK8sApimachineryPkgApisMetaV1APIVersions
        """
        return util.deserialize_model(dikt, cls)

    @property
    def api_version(self) -> str:
        """Gets the api_version of this IoK8sApimachineryPkgApisMetaV1APIVersions.

        APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#resources  # noqa: E501

        :return: The api_version of this IoK8sApimachineryPkgApisMetaV1APIVersions.
        :rtype: str
        """
        return self._api_version

    @api_version.setter
    def api_version(self, api_version: str):
        """Sets the api_version of this IoK8sApimachineryPkgApisMetaV1APIVersions.

        APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#resources  # noqa: E501

        :param api_version: The api_version of this IoK8sApimachineryPkgApisMetaV1APIVersions.
        :type api_version: str
        """

        self._api_version = api_version

    @property
    def kind(self) -> str:
        """Gets the kind of this IoK8sApimachineryPkgApisMetaV1APIVersions.

        Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#types-kinds  # noqa: E501

        :return: The kind of this IoK8sApimachineryPkgApisMetaV1APIVersions.
        :rtype: str
        """
        return self._kind

    @kind.setter
    def kind(self, kind: str):
        """Sets the kind of this IoK8sApimachineryPkgApisMetaV1APIVersions.

        Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#types-kinds  # noqa: E501

        :param kind: The kind of this IoK8sApimachineryPkgApisMetaV1APIVersions.
        :type kind: str
        """

        self._kind = kind

    @property
    def server_address_by_client_cid_rs(self) -> List[IoK8sApimachineryPkgApisMetaV1ServerAddressByClientCIDR]:
        """Gets the server_address_by_client_cid_rs of this IoK8sApimachineryPkgApisMetaV1APIVersions.

        a map of client CIDR to server address that is serving this group. This is to help clients reach servers in the most network-efficient way possible. Clients can use the appropriate server address as per the CIDR that they match. In case of multiple matches, clients should use the longest matching CIDR. The server returns only those CIDRs that it thinks that the client can match. For example: the master will return an internal IP CIDR only, if the client reaches the server using an internal IP. Server looks at X-Forwarded-For header or X-Real-Ip header or request.RemoteAddr (in that order) to get the client IP.  # noqa: E501

        :return: The server_address_by_client_cid_rs of this IoK8sApimachineryPkgApisMetaV1APIVersions.
        :rtype: List[IoK8sApimachineryPkgApisMetaV1ServerAddressByClientCIDR]
        """
        return self._server_address_by_client_cid_rs

    @server_address_by_client_cid_rs.setter
    def server_address_by_client_cid_rs(self, server_address_by_client_cid_rs: List[IoK8sApimachineryPkgApisMetaV1ServerAddressByClientCIDR]):
        """Sets the server_address_by_client_cid_rs of this IoK8sApimachineryPkgApisMetaV1APIVersions.

        a map of client CIDR to server address that is serving this group. This is to help clients reach servers in the most network-efficient way possible. Clients can use the appropriate server address as per the CIDR that they match. In case of multiple matches, clients should use the longest matching CIDR. The server returns only those CIDRs that it thinks that the client can match. For example: the master will return an internal IP CIDR only, if the client reaches the server using an internal IP. Server looks at X-Forwarded-For header or X-Real-Ip header or request.RemoteAddr (in that order) to get the client IP.  # noqa: E501

        :param server_address_by_client_cid_rs: The server_address_by_client_cid_rs of this IoK8sApimachineryPkgApisMetaV1APIVersions.
        :type server_address_by_client_cid_rs: List[IoK8sApimachineryPkgApisMetaV1ServerAddressByClientCIDR]
        """
        if server_address_by_client_cid_rs is None:
            raise ValueError("Invalid value for `server_address_by_client_cid_rs`, must not be `None`")  # noqa: E501

        self._server_address_by_client_cid_rs = server_address_by_client_cid_rs

    @property
    def versions(self) -> List[str]:
        """Gets the versions of this IoK8sApimachineryPkgApisMetaV1APIVersions.

        versions are the api versions that are available.  # noqa: E501

        :return: The versions of this IoK8sApimachineryPkgApisMetaV1APIVersions.
        :rtype: List[str]
        """
        return self._versions

    @versions.setter
    def versions(self, versions: List[str]):
        """Sets the versions of this IoK8sApimachineryPkgApisMetaV1APIVersions.

        versions are the api versions that are available.  # noqa: E501

        :param versions: The versions of this IoK8sApimachineryPkgApisMetaV1APIVersions.
        :type versions: List[str]
        """
        if versions is None:
            raise ValueError("Invalid value for `versions`, must not be `None`")  # noqa: E501

        self._versions = versions
