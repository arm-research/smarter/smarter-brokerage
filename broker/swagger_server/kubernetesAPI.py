from kubernetes import client, config
from flask import g

def get_kubecon():
    kubecon = getattr(g, '_kubernetes', None)
    if kubecon is None:
        config.load_kube_config()

        kubecon = client.CoreV1Api()
    return kubecon

#@app.teardown_appcontext
#def close_connection(exception):
#    kubecon = getattr(g, '_kubernetes', None)
#    if kubecon is not None:
#        db.close()

