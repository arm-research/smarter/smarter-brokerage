# coding: utf-8

from __future__ import absolute_import

from flask import json
from six import BytesIO

from swagger_server.models.arm_brokerage_api_core_v1alpha1_node import ArmBrokerageApiCoreV1alpha1Node  # noqa: E501
from swagger_server.models.arm_brokerage_api_core_v1alpha1_node_list import ArmBrokerageApiCoreV1alpha1NodeList  # noqa: E501
from swagger_server.models.arm_brokerage_api_core_v1alpha1_physical_node import ArmBrokerageApiCoreV1alpha1PhysicalNode  # noqa: E501
from swagger_server.models.arm_brokerage_api_core_v1alpha1_physical_node_list import ArmBrokerageApiCoreV1alpha1PhysicalNodeList  # noqa: E501
from swagger_server.models.arm_brokerage_api_core_v1alpha1_tenant import ArmBrokerageApiCoreV1alpha1Tenant  # noqa: E501
from swagger_server.models.arm_brokerage_api_core_v1alpha1_tenant_list import ArmBrokerageApiCoreV1alpha1TenantList  # noqa: E501
from swagger_server.models.io_k8s_apimachinery_pkg_apis_meta_v1_status import IoK8sApimachineryPkgApisMetaV1Status  # noqa: E501
from swagger_server.test import BaseTestCase


class TestCoreV1alpha1Controller(BaseTestCase):
    """CoreV1alpha1Controller integration test stubs"""

    def test_create_core_v1alpha1_node(self):
        """Test case for create_core_v1alpha1_node

        
        """
        body = ArmBrokerageApiCoreV1alpha1Node()
        query_string = [('pretty', 'pretty_example'),
                        ('dry_run', 'dry_run_example'),
                        ('field_manager', 'field_manager_example')]
        response = self.client.open(
            '/tenants/{tenantID}/nodes'.format(tenant_id='tenant_id_example'),
            method='POST',
            data=json.dumps(body),
            content_type='application/json',
            query_string=query_string)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_create_core_v1alpha1_physical_node(self):
        """Test case for create_core_v1alpha1_physical_node

        
        """
        body = ArmBrokerageApiCoreV1alpha1PhysicalNode()
        query_string = [('pretty', 'pretty_example'),
                        ('dry_run', 'dry_run_example'),
                        ('field_manager', 'field_manager_example')]
        response = self.client.open(
            '/physicalnodes',
            method='POST',
            data=json.dumps(body),
            content_type='application/json',
            query_string=query_string)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_create_core_v1alpha1_tenant(self):
        """Test case for create_core_v1alpha1_tenant

        
        """
        body = ArmBrokerageApiCoreV1alpha1Tenant()
        query_string = [('pretty', 'pretty_example'),
                        ('dry_run', 'dry_run_example'),
                        ('field_manager', 'field_manager_example')]
        response = self.client.open(
            '/tenants',
            method='POST',
            data=json.dumps(body),
            content_type='application/json',
            query_string=query_string)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_delete_core_v1alpha1_collection_node(self):
        """Test case for delete_core_v1alpha1_collection_node

        
        """
        query_string = [('pretty', 'pretty_example'),
                        ('_continue', '_continue_example'),
                        ('dry_run', 'dry_run_example'),
                        ('field_selector', 'field_selector_example'),
                        ('grace_period_seconds', 56),
                        ('label_selector', 'label_selector_example'),
                        ('limit', 56),
                        ('orphan_dependents', true),
                        ('propagation_policy', 'propagation_policy_example'),
                        ('resource_version', 'resource_version_example'),
                        ('resource_version_match', 'resource_version_match_example'),
                        ('timeout_seconds', 56)]
        response = self.client.open(
            '/tenants/{tenantID}/nodes'.format(tenant_id='tenant_id_example'),
            method='DELETE',
            query_string=query_string)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_delete_core_v1alpha1_collection_physical_node(self):
        """Test case for delete_core_v1alpha1_collection_physical_node

        
        """
        query_string = [('pretty', 'pretty_example'),
                        ('_continue', '_continue_example'),
                        ('dry_run', 'dry_run_example'),
                        ('field_selector', 'field_selector_example'),
                        ('grace_period_seconds', 56),
                        ('label_selector', 'label_selector_example'),
                        ('limit', 56),
                        ('orphan_dependents', true),
                        ('propagation_policy', 'propagation_policy_example'),
                        ('resource_version', 'resource_version_example'),
                        ('resource_version_match', 'resource_version_match_example'),
                        ('timeout_seconds', 56)]
        response = self.client.open(
            '/physicalnodes',
            method='DELETE',
            query_string=query_string)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_delete_core_v1alpha1_node(self):
        """Test case for delete_core_v1alpha1_node

        
        """
        query_string = [('pretty', 'pretty_example'),
                        ('dry_run', 'dry_run_example'),
                        ('grace_period_seconds', 56),
                        ('orphan_dependents', true),
                        ('propagation_policy', 'propagation_policy_example')]
        response = self.client.open(
            '/tenants/{tenantID}/nodes/{nodeName}'.format(tenant_id='tenant_id_example', node_name='node_name_example'),
            method='DELETE',
            query_string=query_string)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_delete_core_v1alpha1_physical_node(self):
        """Test case for delete_core_v1alpha1_physical_node

        
        """
        query_string = [('pretty', 'pretty_example'),
                        ('dry_run', 'dry_run_example'),
                        ('grace_period_seconds', 56),
                        ('orphan_dependents', true),
                        ('propagation_policy', 'propagation_policy_example')]
        response = self.client.open(
            '/physicalnodes/{nodeName}'.format(node_name='node_name_example'),
            method='DELETE',
            query_string=query_string)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_delete_core_v1alpha1_tenant(self):
        """Test case for delete_core_v1alpha1_tenant

        
        """
        query_string = [('pretty', 'pretty_example'),
                        ('dry_run', 'dry_run_example'),
                        ('grace_period_seconds', 56),
                        ('orphan_dependents', true),
                        ('propagation_policy', 'propagation_policy_example')]
        response = self.client.open(
            '/tenants/{tenantID}'.format(tenant_id='tenant_id_example'),
            method='DELETE',
            query_string=query_string)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_list_core_v1alpha1_node(self):
        """Test case for list_core_v1alpha1_node

        
        """
        query_string = [('pretty', 'pretty_example'),
                        ('allow_watch_bookmarks', true),
                        ('_continue', '_continue_example'),
                        ('field_selector', 'field_selector_example'),
                        ('label_selector', 'label_selector_example'),
                        ('limit', 56),
                        ('resource_version', 'resource_version_example'),
                        ('resource_version_match', 'resource_version_match_example'),
                        ('timeout_seconds', 56)]
        response = self.client.open(
            '/tenants/{tenantID}/nodes'.format(tenant_id='tenant_id_example'),
            method='GET',
            query_string=query_string)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_list_core_v1alpha1_physical_node(self):
        """Test case for list_core_v1alpha1_physical_node

        
        """
        query_string = [('pretty', 'pretty_example'),
                        ('allow_watch_bookmarks', true),
                        ('_continue', '_continue_example'),
                        ('field_selector', 'field_selector_example'),
                        ('label_selector', 'label_selector_example'),
                        ('limit', 56),
                        ('resource_version', 'resource_version_example'),
                        ('resource_version_match', 'resource_version_match_example'),
                        ('timeout_seconds', 56)]
        response = self.client.open(
            '/physicalnodes',
            method='GET',
            query_string=query_string)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_list_core_v1alpha1_tenants(self):
        """Test case for list_core_v1alpha1_tenants

        
        """
        query_string = [('pretty', 'pretty_example'),
                        ('allow_watch_bookmarks', true),
                        ('_continue', '_continue_example'),
                        ('field_selector', 'field_selector_example'),
                        ('label_selector', 'label_selector_example'),
                        ('limit', 56),
                        ('resource_version', 'resource_version_example'),
                        ('resource_version_match', 'resource_version_match_example'),
                        ('timeout_seconds', 56)]
        response = self.client.open(
            '/tenants',
            method='GET',
            query_string=query_string)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_read_core_v1alpha1_node(self):
        """Test case for read_core_v1alpha1_node

        
        """
        query_string = [('pretty', 'pretty_example'),
                        ('exact', true),
                        ('export', true)]
        response = self.client.open(
            '/tenants/{tenantID}/nodes/{nodeName}'.format(tenant_id='tenant_id_example', node_name='node_name_example'),
            method='GET',
            query_string=query_string)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_read_core_v1alpha1_node_status(self):
        """Test case for read_core_v1alpha1_node_status

        
        """
        query_string = [('pretty', 'pretty_example')]
        response = self.client.open(
            '/tenants/{tenantID}/nodes/{nodeName}/status'.format(tenant_id='tenant_id_example', node_name='node_name_example'),
            method='GET',
            query_string=query_string)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_read_core_v1alpha1_physical_node(self):
        """Test case for read_core_v1alpha1_physical_node

        
        """
        query_string = [('pretty', 'pretty_example'),
                        ('exact', true),
                        ('export', true)]
        response = self.client.open(
            '/physicalnodes/{nodeName}'.format(node_name='node_name_example'),
            method='GET',
            query_string=query_string)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_read_core_v1alpha1_physical_node_status(self):
        """Test case for read_core_v1alpha1_physical_node_status

        
        """
        query_string = [('pretty', 'pretty_example')]
        response = self.client.open(
            '/physicalnodes/{nodeName}/status'.format(node_name='node_name_example'),
            method='GET',
            query_string=query_string)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_read_core_v1alpha1_tenant(self):
        """Test case for read_core_v1alpha1_tenant

        
        """
        query_string = [('pretty', 'pretty_example'),
                        ('exact', true),
                        ('export', true)]
        response = self.client.open(
            '/tenants/{tenantID}'.format(tenant_id='tenant_id_example'),
            method='GET',
            query_string=query_string)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_replace_core_v1alpha1_node(self):
        """Test case for replace_core_v1alpha1_node

        
        """
        body = ArmBrokerageApiCoreV1alpha1Node()
        query_string = [('pretty', 'pretty_example'),
                        ('dry_run', 'dry_run_example'),
                        ('field_manager', 'field_manager_example')]
        response = self.client.open(
            '/tenants/{tenantID}/nodes/{nodeName}'.format(tenant_id='tenant_id_example', node_name='node_name_example'),
            method='PUT',
            data=json.dumps(body),
            content_type='application/json',
            query_string=query_string)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_replace_core_v1alpha1_node_status(self):
        """Test case for replace_core_v1alpha1_node_status

        
        """
        body = ArmBrokerageApiCoreV1alpha1Node()
        query_string = [('pretty', 'pretty_example'),
                        ('dry_run', 'dry_run_example'),
                        ('field_manager', 'field_manager_example')]
        response = self.client.open(
            '/tenants/{tenantID}/nodes/{nodeName}/status'.format(tenant_id='tenant_id_example', node_name='node_name_example'),
            method='PUT',
            data=json.dumps(body),
            content_type='application/json',
            query_string=query_string)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_replace_core_v1alpha1_physical_node(self):
        """Test case for replace_core_v1alpha1_physical_node

        
        """
        body = ArmBrokerageApiCoreV1alpha1PhysicalNode()
        query_string = [('pretty', 'pretty_example'),
                        ('dry_run', 'dry_run_example'),
                        ('field_manager', 'field_manager_example')]
        response = self.client.open(
            '/physicalnodes/{nodeName}'.format(node_name='node_name_example'),
            method='PUT',
            data=json.dumps(body),
            content_type='application/json',
            query_string=query_string)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_replace_core_v1alpha1_physical_node_status(self):
        """Test case for replace_core_v1alpha1_physical_node_status

        
        """
        body = ArmBrokerageApiCoreV1alpha1PhysicalNode()
        query_string = [('pretty', 'pretty_example'),
                        ('dry_run', 'dry_run_example'),
                        ('field_manager', 'field_manager_example')]
        response = self.client.open(
            '/physicalnodes/{nodeName}/status'.format(node_name='node_name_example'),
            method='PUT',
            data=json.dumps(body),
            content_type='application/json',
            query_string=query_string)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_replace_core_v1alpha1_tenant(self):
        """Test case for replace_core_v1alpha1_tenant

        
        """
        body = ArmBrokerageApiCoreV1alpha1Tenant()
        query_string = [('pretty', 'pretty_example'),
                        ('dry_run', 'dry_run_example'),
                        ('field_manager', 'field_manager_example')]
        response = self.client.open(
            '/tenants/{tenantID}'.format(tenant_id='tenant_id_example'),
            method='PUT',
            data=json.dumps(body),
            content_type='application/json',
            query_string=query_string)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))


if __name__ == '__main__':
    import unittest
    unittest.main()
