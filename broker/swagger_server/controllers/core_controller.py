import connexion
import six

from swagger_server.models.io_k8s_apimachinery_pkg_apis_meta_v1_api_versions import IoK8sApimachineryPkgApisMetaV1APIVersions  # noqa: E501
from swagger_server import util


def get_core_api_versions():  # noqa: E501
    """get_core_api_versions

    get available API versions # noqa: E501


    :rtype: IoK8sApimachineryPkgApisMetaV1APIVersions
    """

    response = IoK8sApimachineryPkgApisMetaV1APIVersions(api_version="v1alpha1",kind="APIVersions", server_address_by_client_cid_rs=[], versions=["v1alpha1"]) # noqa: E501
    return response
