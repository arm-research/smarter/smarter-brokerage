import connexion
import six
import swagger_server.kubernetesAPI
import base64

from flask import json

from swagger_server.models.arm_brokerage_api_core_v1alpha1_node import ArmBrokerageApiCoreV1alpha1Node  # noqa: E501
from swagger_server.models.arm_brokerage_api_core_v1alpha1_node_list import ArmBrokerageApiCoreV1alpha1NodeList  # noqa: E501
from swagger_server.models.arm_brokerage_api_core_v1alpha1_physical_node import ArmBrokerageApiCoreV1alpha1PhysicalNode  # noqa: E501
from swagger_server.models.arm_brokerage_api_core_v1alpha1_physical_node_list import ArmBrokerageApiCoreV1alpha1PhysicalNodeList  # noqa: E501
from swagger_server.models.arm_brokerage_api_core_v1alpha1_tenant import ArmBrokerageApiCoreV1alpha1Tenant  # noqa: E501
from swagger_server.models.arm_brokerage_api_core_v1alpha1_tenant_list import ArmBrokerageApiCoreV1alpha1TenantList  # noqa: E501
from swagger_server.models.arm_brokerage_api_core_v1alpha1_server_credential import ArmBrokerageApiCoreV1alpha1ServerCredential
from swagger_server.models.io_k8s_apimachinery_pkg_apis_meta_v1_api_resource_list import IoK8sApimachineryPkgApisMetaV1APIResourceList,IoK8sApimachineryPkgApisMetaV1APIResource  # noqa: E501
from swagger_server.models.io_k8s_apimachinery_pkg_apis_meta_v1_status import IoK8sApimachineryPkgApisMetaV1Status  # noqa: E501
from swagger_server.models.io_k8s_apimachinery_pkg_apis_meta_v1_status_details import IoK8sApimachineryPkgApisMetaV1StatusDetails  # noqa: E501
from swagger_server import util

def getAccountType():

    accountType = connexion.request.headers.get('X-AccountRole')

    if not accountType:
        print ('No account role',connexion.request.headers)
        return False,'Unauthorized: X-AccountRole required',None

    if accountType == 'broker':
        return True,accountType,None

    if accountType == 'tenant':
       accountID = connexion.request.headers.get('X-AccountID')
       if not accountID:
           print ('No account ID')
           return False,'Unauthorized: X-AccountID required',None

       return True,accountType,accountID

    print ('Unauthorized role:'+accountType)
    return False, 'Unauthorized role:'+accountType, None

def encode64k8s(inputString):
    return 'B'+base64.b64encode(bytes(inputString,'utf-8'),bytes('-_','utf-8')).decode('utf-8').replace('=','.')+'B'

def decode64k8s(inputString):
    return base64.b64decode(bytes(inputString[1:-1].replace('.','='),'utf-8'),bytes('-_','utf-8')).decode('utf-8')

def credential_validate(credential):
    if not credential.certificate_authority is None and (credential.username is None or credential.password is None):
        return False

    if credential.token is None and credential.certificate_authority is None:
        return False

    return True

def credentialFromAnnotations(annotations):
    if not isinstance(annotations,dict):
       return None
    credentialReturn = ArmBrokerageApiCoreV1alpha1ServerCredential(server = annotations['server'])
    if 'token' in annotations:
        credentialReturn.token = annotations['token']
    if 'certificateAuthority' in annotations:
        credentialReturn.certificate_authority = annotations['certificateAuthority']
    if 'username' in annotations:
        credentialReturn.username = annotations['username']
    if 'password`' in annotations:
        credentialReturn.password = annotations['password`']
    return credentialReturn

def annotationsFromCredential(credential):
    credAnnotations = {'server': credential.server}
    if not credential.token is None:
       credAnnotations['token'] = credential.token
    if not credential.certificate_authority is None:
       credAnnotations['certificateAuthority'] = credential.certificate_authority
    if not credential.username is None:
       credAnnotations['username'] = credential.username
    if not credential.password is None:
       credAnnotations['password'] = credential.password
    return credAnnotations

def resourcesFromAnnotations(annotations):
    if not isinstance(annotations,dict):
       return None
    if not ( 'resourcesLimits' in annotations or 'resourcesRequests' in annotations):
       return None
    resourcesReturn = {}
    if 'resourcesLimits' in annotations:
       resourcesReturn['limits'] =  json.load(annotations['resourcesLimits'])
    if 'resourcesRequests' in annotations:
       resourcesReturn['requests'] = json.load(annotations['resourcesRequests'])

    return resourcesReturn

def reourcesfromV1Resources(resourcesPod):
    if not isinstance(resourcesPod,swagger_server.kubernetesAPI.client.V1ResourceRequirements):
        return None
    return { 'limits': resourcesPod.limits, 'requests': resourcesPod.requests}

def podTestInfo(tenant_id,nodeInfo,resources):
    return swagger_server.kubernetesAPI.client.V1Pod(
                        metadata = swagger_server.kubernetesAPI.client.V1ObjectMeta(
                                  name = nodeInfo.id, 
                                  namespace = tenant_id, 
                                  labels = { "physicalname":nodeInfo.id },
                                  annotations = annotationsFromCredential(nodeInfo.credential)
                        ),
                        spec = swagger_server.kubernetesAPI.client.V1PodSpec(
                                  service_account_name = "default",
                                  automount_service_account_token = False,
                                  dns_policy = "ClusterFirstWithHostNet",
                                  hostname = nodeInfo.id,
                                  node_name = nodeInfo.id,
                                  restart_policy = "Never",
                                  containers = [
                                          swagger_server.kubernetesAPI.client.V1Container(
                                                  name = "virtual-node-container",
                                                  image_pull_policy = "IfNotPresent",
                                                  image = "alpine",
                                                  command = [ "/bin/ash" ],
                                                  args = [
                                                          "-c",
                                                          "echo \"Server \"${K3S_SERVER}\" Token \"${K3S_TOKEN};while true; do sleep 1000;done;exit 0"
                                                  ],
                                                  env = [
                                                          swagger_server.kubernetesAPI.client.V1EnvVar(
                                                                   name = "K3S_SERVER",
                                                                   value = nodeInfo.credential.server),
                                                          swagger_server.kubernetesAPI.client.V1EnvVar(
                                                                   name = "K3S_TOKEN",
                                                                   value = nodeInfo.credential.token)
                                                  ],
                                                  resources = resources
                                          )
                                  ],
                                  tolerations = [ 
                                          swagger_server.kubernetesAPI.client.V1Toleration(
                                                  key = "smarter.type",
                                                  operator = "Equal",
                                                  value = "edge",
                                                  effect = "NoSchedule"
                                          )
                                  ],
                                  termination_grace_period_seconds = 10
                        )
              )

def podDindInfo(tenant_id,nodeInfo,nodetype):
    return swagger_server.kubernetesAPI.client.V1Pod(
                        metadata = swagger_server.kubernetesAPI.client.V1ObjectMeta(
                                  name = nodeInfo.id, 
                                  namespace = tenant_id, 
                                  labels = { "physicalname":nodeInfo.physical_node },
                                  annotations = annotationsFromCredential(nodeInfo.credential)
                        ),
                        spec = swagger_server.kubernetesAPI.client.V1PodSpec(
                                  service_account_name = "default",
                                  automount_service_account_token = False,
                                  dns_policy = "ClusterFirstWithHostNet",
                                  hostname = nodeInfo.id+"-"+tenant_id,
                                  node_name = nodeInfo.physical_node,
                                  restart_policy = "Always",
                                  containers = [
                                          swagger_server.kubernetesAPI.client.V1Container(
                                                  name = "virtual-node-container",
                                                  image_pull_policy = "IfNotPresent",
                                                  image = "registry.gitlab.com/arm-research/smarter/smarter-dind:v0.9.2",
                                                  env = [
                                                          swagger_server.kubernetesAPI.client.V1EnvVar(
                                                                   name = "K3S_SERVER",
                                                                   value = nodeInfo.credential.server),
                                                          swagger_server.kubernetesAPI.client.V1EnvVar(
                                                                   name = "K3S_TOKEN",
                                                                   value = nodeInfo.credential.token),
                                                          swagger_server.kubernetesAPI.client.V1EnvVar(
                                                                   name = "K3S_CLUSTER_DNS",
                                                                   value = "169.254.1.2"),
                                                          swagger_server.kubernetesAPI.client.V1EnvVar(
                                                                   name = "K3S_NODENAME",
                                                                   value = nodeInfo.id),
                                                          swagger_server.kubernetesAPI.client.V1EnvVar(
                                                                   name = "K3S_NODETYPE",
                                                                   value = nodetype)
                                                  ],
                                                  resources = swagger_server.kubernetesAPI.client.V1ResourceRequirements(
                                                          limits = nodeInfo.resources['limits'],
                                                          requests = nodeInfo.resources['requests']
                                                  ),
                                                  security_context = swagger_server.kubernetesAPI.client.V1SecurityContext(
                                                          privileged = True
                                                  ),
                                                  volume_mounts = [
                                                          swagger_server.kubernetesAPI.client.V1VolumeMount(
                                                                mount_path = "/var/lib/rancher/k3s",
                                                                name = "k3s-smarter-server"),
                                                          swagger_server.kubernetesAPI.client.V1VolumeMount(
                                                                mount_path = "/var/lib/docker",
                                                                name = "docker-smarter-server")
                                                  ]

                                          )
                                  ],
                                  volumes = [
                                          swagger_server.kubernetesAPI.client.V1Volume(
                                                  name = "k3s-smarter-server",
                                                  host_path = swagger_server.kubernetesAPI.client.V1HostPathVolumeSource(
                                                          path = "/persistent/virtual-node/"+tenant_id+"-"+nodeInfo.id+"/k3s",
                                                          type = "DirectoryOrCreate")
                                          ),
                                          swagger_server.kubernetesAPI.client.V1Volume(
                                                  name = "docker-smarter-server",
                                                  host_path = swagger_server.kubernetesAPI.client.V1HostPathVolumeSource(
                                                          path = "/persistent/virtual-node/"+tenant_id+"-"+nodeInfo.id+"/docker",
                                                          type = "DirectoryOrCreate")
                                          )
                                  ],
                                  tolerations = [ 
                                          swagger_server.kubernetesAPI.client.V1Toleration(
                                                  key = "smarter.type",
                                                  operator = "Equal",
                                                  value = "edge",
                                                  effect = "NoSchedule"
                                          )
                                  ],
                                  termination_grace_period_seconds = 10
                        )
              )

def create_core_v1alpha1_node(body, tenant_id, pretty=None, dry_run=None, field_manager=None):  # noqa: E501
    """create_core_v1alpha1_node

    create a Node # noqa: E501

    :param body: 
    :type body: dict | bytes
    :param tenant_id: name of the Tenant
    :type tenant_id: str
    :param pretty: If &#x27;true&#x27;, then the output is pretty printed.
    :type pretty: str
    :param dry_run: When present, indicates that modifications should not be persisted. An invalid or unrecognized dryRun directive will result in an error response and no further processing of the request. Valid values are: - All: all dry run stages will be processed
    :type dry_run: str
    :param field_manager: fieldManager is a name associated with the actor or entity that is making these changes. The value must be less than or 128 characters long, and only contain printable characters, as defined by https://golang.org/pkg/unicode/#IsPrint.
    :type field_manager: str

    :rtype: ArmBrokerageApiCoreV1alpha1Node
    """
    if connexion.request.is_json:
        body = ArmBrokerageApiCoreV1alpha1Node.from_dict(connexion.request.get_json())  # noqa: E501

        resOK,accountType,accountID = getAccountType()

        if not resOK:
            return (accountType,401)

        if accountType == 'tenant' and accountID != tenant_id:
            print ('Unauthorized tenant:'+accountID)
            return ('Unauthorized tenant:'+accountID,401)
 
        if isinstance(body.credential,dict):
            if not credential_validate(body.credential):
                return (IoK8sApimachineryPkgApisMetaV1Status(
                                        kind="Status",
                                        api_version="v1alpha1",
                                        metadata={},
                                        status="Failure",
                                        message="credential is not correct",
                                        reason="input error",
                                        details=IoK8sApimachineryPkgApisMetaV1StatusDetails(
                                                                name=body.id,
                                                                kind="node"
                                                                ),
                                        code=400
                                        ),
                        400)

        kubecon = swagger_server.kubernetesAPI.get_kubecon()

        try:
            namespaceFound = kubecon.read_namespace(tenant_id)

        except swagger_server.kubernetesAPI.client.rest.ApiException as e:
            #print("Exception when calling CoreV1Api->read_namespace: %s\n" % e)
            #statusK8s = json.loads(e.body,object_hook=swagger_server.kubernetesAPI.client.V1Status);
            if e.status == 404:
               return (IoK8sApimachineryPkgApisMetaV1Status(
                                kind="Status",
                                api_version="v1alpha1",
                                metadata={},
                                status="Failure",
                                message="tenant \""+tenant_id+"\" not found",
                                reason="NotFound",
                                details=IoK8sApimachineryPkgApisMetaV1StatusDetails(
                                                        name=tenant_id,
                                                        kind="tenant"),
                                code=409
                                ),
                        409)

            print("Exception when calling CoreV1Api->create_namespace: %s\n" % e)
            return (IoK8sApimachineryPkgApisMetaV1Status(
                                kind="Status",
                                api_version="v1alpha1",
                                metadata={},
                                status="Failure",
                                message="tenant \""+tenant_id+"\" error",
                                reason="NotFound",
                                details=IoK8sApimachineryPkgApisMetaV1StatusDetails(
                                                        name=tenant_id,
                                                        kind="tenant"),
                                code=e.status
                                ),
                    e.status) 

        #print("namespace:", namespaceFound)
        #print("type(namespaceFound.metadata.labels):",type(namespaceFound.metadata.labels))

        if not isinstance(namespaceFound.metadata.labels,dict):
            return (IoK8sApimachineryPkgApisMetaV1Status(
                                kind="Status",
                                api_version="v1alpha1",
                                metadata={},
                                status="Failure",
                                message="tenant \""+tenant_id+"\" not a tenant",
                                reason="NotFound",
                                details=IoK8sApimachineryPkgApisMetaV1StatusDetails(
                                                        name=tenant_id,
                                                        kind="tenant"),
                                code=409),
                    409)
        if not 'tenantName' in namespaceFound.metadata.labels:
            return (IoK8sApimachineryPkgApisMetaV1Status(
                                kind="Status",
                                api_version="v1alpha1",
                                metadata={},
                                status="Failure",
                                message="tenant \""+tenant_id+"\" not a tenant",
                                reason="NotFound",
                                details=IoK8sApimachineryPkgApisMetaV1StatusDetails(
                                                        name=tenant_id,
                                                        kind="tenant"),
                                code=409),
                    409)

        if isinstance(body.credential,dict):
            if not credential_validate(body.credential):
               return (IoK8sApimachineryPkgApisMetaV1Status(
                                        kind="Status",
                                        api_version="v1alpha1",
                                        metadata={},
                                        status="Failure",
                                        message="Node has invalid credentials",
                                        reason="NotFound",
                                        details=IoK8sApimachineryPkgApisMetaV1StatusDetails(
                                                        name=body.id,
                                                        kind="node"),
                                        code=400),
                      400)
        else:
            body.credential = credentialFromAnnotations(namespaceFound.metadata.annotations)

        if not isinstance(body.physical_node,str):
            body.physical_node = body.id

        try:
            k8sNode = kubecon.read_node(name = body.physical_node)
        except swagger_server.kubernetesAPI.client.rest.ApiException as e:
            #print("Exception when calling CoreV1Api->read_namespace: %s\n" % e)
            #statusK8s = json.loads(e.body,object_hook=swagger_server.kubernetesAPI.client.V1Status);
            if e.status == 404:
               return (IoK8sApimachineryPkgApisMetaV1Status(
                                        kind="Status",
                                        api_version="v1alpha1",
                                        metadata={},
                                        status="Failure",
                                        message="physical node \""+body.physical_node+"\" not found",
                                        reason="NotFound",
                                        details=IoK8sApimachineryPkgApisMetaV1StatusDetails(
                                                        name=body.id,
                                                        kind="node"),
                                        code=422),

                       422)
            return (IoK8sApimachineryPkgApisMetaV1Status(
                                kind="Status",
                                api_version="v1alpha1",
                                metadata={},
                                status="Failure",
                                message="physical node \""+body.physical_node+"\" error",
                                reason="NotFound",
                                details=IoK8sApimachineryPkgApisMetaV1StatusDetails(
                                                name=body.id,
                                                kind="physicalNode"),
                                code=422),
                    422)

        if not isinstance(k8sNode.metadata.labels,dict):
            #print('k8s node:',k8sNode)
            return (IoK8sApimachineryPkgApisMetaV1Status(
                                kind="Status",
                                api_version="v1alpha1",
                                metadata={},
                                status="Failure",
                                message="physical node \""+body.physical_node+"\" not enabled",
                                reason="NotFound",
                                details=IoK8sApimachineryPkgApisMetaV1StatusDetails(
                                                name=tenant_id,
                                kind="physicalNode"),
                                code=422),
                    422)
        if not 'physicalname' in k8sNode.metadata.labels:
            #print('k8s node:',k8sNode)
            return (IoK8sApimachineryPkgApisMetaV1Status(
                                kind="Status",
                                api_version="v1alpha1",
                                metadata={},
                                status="Failure",
                                message="physical node \""+body.physical_node+"\" not enabled",
                                reason="NotFound",
                                details=IoK8sApimachineryPkgApisMetaV1StatusDetails(
                                                name=tenant_id,
                                kind="physicalNode"),
                                code=422),
                    422)

        if 'nodetype' in k8sNode.metadata.labels:
            nodetype = k8sNode.metadata.labels['nodetype']
        else:
            nodetype = 'virtual'

        resourcesDefault = swagger_server.kubernetesAPI.client.V1ResourceRequirements(
                                          limits = {
                                                  "cpu": "2000m",
                                                  "memory": "2000Mi"
                                          },
                                          requests = {
                                                  "cpu": "100m",
                                                  "memory": "100Mi"
                                          }
                                  )

        if 'resourcesLimits' in namespaceFound.metadata.annotations:
            resourcesDefault.limits.update(json.load(namespaceFound.metadata.annotations['resourcesLimits']))
        if 'resourcesRequests' in namespaceFound.metadata.annotations:
            resourcesDefault.requests.update(json.load(namespaceFound.metadata.annotations['resourcesRequests']))


        if isinstance(body.resources,dict):
            if 'limits' in body.resources:
                resourcesDefault.limits.update(body.resources['limits'])
            if 'requests' in body.resources:
                resourcesDefault.limits.update(body.resources['requests'])

        if not isinstance(body.resources,dict):
            body.resources = { 'limits': resourcesDefault.limits, 'requests': resourcesDefault.requests }
        else:
            body.resources['limits'] = resourcesDefault.limits
            body.resources['requests'] = resourcesDefault.requests
        
        #podNew = podTestInfo(tenant_id,body)
        podNew = podDindInfo(tenant_id,body,nodetype)

        try:
            podCreated = kubecon.create_namespaced_pod(tenant_id, podNew)
        except swagger_server.kubernetesAPI.client.rest.ApiException as e:
            print("Exception when calling CoreV1Api->read_namespace: %s\n" % e)
            #statusK8s = json.loads(e.body,object_hook=swagger_server.kubernetesAPI.client.V1Status);
            return (IoK8sApimachineryPkgApisMetaV1Status(
                                kind="Status",
                                api_version="v1alpha1",
                                metadata={},
                                status="Failure",
                                message="node \""+body.id+"\" creation failure",
                                reason="NotFound",
                                details=IoK8sApimachineryPkgApisMetaV1StatusDetails(
                                                        name=body.id,
                                                        kind="node"),
                                code=e.status),
                    e.status)

        return body

    return (IoK8sApimachineryPkgApisMetaV1Status(
                        kind="Status",
                        api_version="v1alpha1",
                        metadata={},
                        status="Failure",
                        message="physical node \""+body.id+"\" not enabled",
                        reason="NotFound",
                        details=IoK8sApimachineryPkgApisMetaV1StatusDetails(
                                                name=tenant_id,
                                                kind="node"),
                        code=404),
            404)


def create_core_v1alpha1_physical_node(body, pretty=None, dry_run=None, field_manager=None):  # noqa: E501
    """create_core_v1alpha1_physical_node

    create a PhysicalNode # noqa: E501

    :param body: 
    :type body: dict | bytes
    :param pretty: If &#x27;true&#x27;, then the output is pretty printed.
    :type pretty: str
    :param dry_run: When present, indicates that modifications should not be persisted. An invalid or unrecognized dryRun directive will result in an error response and no further processing of the request. Valid values are: - All: all dry run stages will be processed
    :type dry_run: str
    :param field_manager: fieldManager is a name associated with the actor or entity that is making these changes. The value must be less than or 128 characters long, and only contain printable characters, as defined by https://golang.org/pkg/unicode/#IsPrint.
    :type field_manager: str

    :rtype: ArmBrokerageApiCoreV1alpha1PhysicalNode
    """
    if connexion.request.is_json:
        body = ArmBrokerageApiCoreV1alpha1PhysicalNode.from_dict(connexion.request.get_json())  # noqa: E501

        resOK,accountType,accountID = getAccountType()

        if not resOK:
            return (accountType,401)

        if accountType != 'broker':
            print('Unauthorized accountRole:'+accountType)
            return ('Unauthorized accountRole:'+accountType,401)

        name64 = encode64k8s(body.name)

        kubecon = swagger_server.kubernetesAPI.get_kubecon()

        try:
            k8sNode = kubecon.read_node(name = body.id)
        except swagger_server.kubernetesAPI.client.rest.ApiException as e:
            #print("Exception when calling CoreV1Api->read_namespace: %s\n" % e)
            #statusK8s = json.loads(e.body,object_hook=swagger_server.kubernetesAPI.client.V1Status);
            if e.status == 404:
               return (IoK8sApimachineryPkgApisMetaV1Status(
                                kind="Status",
                                api_version="v1alpha1",
                                metadata={},
                                status="Failure",
                                message="physical node \""+body.id+"\" not found",
                                reason="NotFound",
                                details=IoK8sApimachineryPkgApisMetaV1StatusDetails(
                                                        name=body.id,
                                                        kind="physicalNode"),
                                code=404),
                                404)
            return (IoK8sApimachineryPkgApisMetaV1Status(
                                kind="Status",
                                api_version="v1alpha1",
                                metadata={},status="Failure",
                                message="physical node \""+body.id+"\" error",
                                reason="NotFound",
                                details=IoK8sApimachineryPkgApisMetaV1StatusDetails(
                                                        name=body.id,
                                                        kind="physicalNode"),
                                code=409),
                                409)

        if isinstance(k8sNode.metadata.labels,dict):
           if 'physicalname' in k8sNode.metadata.labels:
              if name64 == k8sNode.metadata.labels['physicalname']:
                 return body
              return (IoK8sApimachineryPkgApisMetaV1Status(
                                kind="Status",
                                api_version="v1alpha1",
                                metadata={},
                                status="Failure",
                                message="physical node \""+body.id+"\" already enabled with a different name",
                                reason="NotFound",
                                details=IoK8sApimachineryPkgApisMetaV1StatusDetails(
                                name=body.id,
                                kind="physicalNode"),
                                code=409),
                                409)

        #print("Existing node:",k8sNode)
        # Node exists but the label does not so create ito
        nodeNew = {
                "metadata": {
                        "labels" : {
                                "physicalname": name64
                        }
                }
        }

        try:
            #print("Changing node:",nodeNew)
            k8sNode = kubecon.patch_node(name = body.id, body = nodeNew)
        except swagger_server.kubernetesAPI.client.rest.ApiException as e:
            print("Exception when calling CoreV1Api->patch_node: %s\n" % e)
            #statusK8s = json.loads(e.body,object_hook=swagger_server.kubernetesAPI.client.V1Status);
            if e.status == 404:
               return (IoK8sApimachineryPkgApisMetaV1Status(
                                kind="Status",
                                api_version="v1alpha1",
                                metadata={},
                                status="Failure",
                                message="physical node \""+body.id+"\" not found",
                                reason="NotFound",
                                details=IoK8sApimachineryPkgApisMetaV1StatusDetails(
                                                        name=body.id,
                                                        kind="physicalNode"),
                                code=404),
                                404)
            return (IoK8sApimachineryPkgApisMetaV1Status(
                             kind="Status",
                             api_version="v1alpha1",
                             metadata={},
                             status="Failure",
                             message="physical node \""+body.id+"\" error being enabled",
                             reason="NotFound",
                             details=IoK8sApimachineryPkgApisMetaV1StatusDetails(
                                                     name=body.id,
                                                     kind="physicalNode"),
                             code=409),
                             409)

        return body

    return (IoK8sApimachineryPkgApisMetaV1Status(
                     kind="Status",
                     api_version="v1alpha1",
                     metadata={},
                     status="Failure",
                     message="input data is not json",
                     reason="input error",
                     details=IoK8sApimachineryPkgApisMetaV1StatusDetails(
                                             name=body.id,
                                             kind="physicalNode"),
                     code=409),
                     409)

def create_core_v1alpha1_tenant(body, pretty=None, dry_run=None, field_manager=None):  # noqa: E501
    """create_core_v1alpha1_tenant

    create a Tenant # noqa: E501

    :param body: 
    :type body: dict | bytes
    :param pretty: If &#x27;true&#x27;, then the output is pretty printed.
    :type pretty: str
    :param dry_run: When present, indicates that modifications should not be persisted. An invalid or unrecognized dryRun directive will result in an error response and no further processing of the request. Valid values are: - All: all dry run stages will be processed
    :type dry_run: str
    :param field_manager: fieldManager is a name associated with the actor or entity that is making these changes. The value must be less than or 128 characters long, and only contain printable characters, as defined by https://golang.org/pkg/unicode/#IsPrint.
    :type field_manager: str

    :rtype: ArmBrokerageApiCoreV1alpha1Tenant
    """
    if connexion.request.is_json:
        body = ArmBrokerageApiCoreV1alpha1Tenant.from_dict(connexion.request.get_json())  # noqa: E501

        resOK,accountType,accountID = getAccountType()

        if not resOK:
            return (accountType,401)

        if accountType != 'broker':
            print('Unauthorized accountRole:'+accountType)
            return ('Unauthorized accountRole:'+accountType,401)

        if not credential_validate(body.credential):
            statusReturn = IoK8sApimachineryPkgApisMetaV1Status(kind="Status",api_version="v1alpha1",metadata={},status="Failure",message="credential is not correct",reason="input error",details=IoK8sApimachineryPkgApisMetaV1StatusDetails(name=body.id,kind="tenant"),code=400)
            return (statusReturn,400)

        name64 = encode64k8s(body.name)

        kubecon = swagger_server.kubernetesAPI.get_kubecon()

        namespaceNew = swagger_server.kubernetesAPI.client.V1Namespace(
                        metadata = swagger_server.kubernetesAPI.client.V1ObjectMeta(
                                        name = body.id, 
                                        labels = {
                                                'tenantName':name64
                                        }, 
                                        annotations = annotationsFromCredential(body.credential)
                                )
                        )

        if isinstance(body.resources,dict):
            if 'limits' in body.resources:
               namespaceNew.metadata.annotations.update({'resourcesLimits': json.dump(body.resources['limits'])})
            if 'requests' in body.resources:
               namespaceNew.metadata.annotations.update({'resourcesRequests': json.dump(body.resources['requests'])})

        try:
            api_response = kubecon.create_namespace(namespaceNew)
        except swagger_server.kubernetesAPI.client.rest.ApiException as e:
            #print("Exception when calling CoreV1Api->read_namespace: %s\n" % e)
            #statusK8s = json.loads(e.body,object_hook=swagger_server.kubernetesAPI.client.V1Status);
            if e.status == 404:
               statusReturn = IoK8sApimachineryPkgApisMetaV1Status(kind="Status",api_version="v1alpha1",metadata={},status="Failure",message="tenant \""+body.id+"\" not found",reason="NotFound",details=IoK8sApimachineryPkgApisMetaV1StatusDetails(name=body.id,kind="tenant"),code=404)
               return (statusReturn,404)
            statusReturn = IoK8sApimachineryPkgApisMetaV1Status(kind="Status",api_version="v1alpha1",metadata={},status="Failure",message="tenant \""+body.id+"\" error",reason="NotFound",details=IoK8sApimachineryPkgApisMetaV1StatusDetails(name=body.id,kind="tenant"),code=409)
            return (statusReturn,409)
        if isinstance(api_response.metadata.labels,dict):
           if 'tenantName' in api_response.metadata.labels:
              tenantReturn = ArmBrokerageApiCoreV1alpha1Tenant(id = api_response.metadata.name, name = decode64k8s(api_response.metadata.labels['tenantName']), credential = credentialFromAnnotations(api_response.metadata.annotations))
              return tenantReturn
   
        statusReturn = IoK8sApimachineryPkgApisMetaV1Status(kind="Status",api_version="v1alpha1",metadata={},status="Failure",message="tenant \""+body.id+"\" created but not OK",reason="NotFound",details=IoK8sApimachineryPkgApisMetaV1StatusDetails(name=body.id,kind="tenant"),code=409)
        return (statusReturn,409)

    statusReturn = IoK8sApimachineryPkgApisMetaV1Status(kind="Status",api_version="v1alpha1",metadata={},status="Failure",message="input data is not json",reason="input error",details=IoK8sApimachineryPkgApisMetaV1StatusDetails(name=body.id,kind="physicalNode"),code=409)
    return (statusReturn,409)


def delete_core_v1alpha1_collection_node(tenant_id, pretty=None, _continue=None, dry_run=None, field_selector=None, grace_period_seconds=None, label_selector=None, limit=None, orphan_dependents=None, propagation_policy=None, resource_version=None, resource_version_match=None, timeout_seconds=None):  # noqa: E501
    """delete_core_v1alpha1_collection_node

    delete collection of Node # noqa: E501

    :param tenant_id: name of the Tenant
    :type tenant_id: str
    :param pretty: If &#x27;true&#x27;, then the output is pretty printed.
    :type pretty: str
    :param _continue: The continue option should be set when retrieving more results from the server. Since this value is server defined, clients may only use the continue value from a previous query result with identical query parameters (except for the value of continue) and the server may reject a continue value it does not recognize. If the specified continue value is no longer valid whether due to expiration (generally five to fifteen minutes) or a configuration change on the server, the server will respond with a 410 ResourceExpired error together with a continue token. If the client needs a consistent list, it must restart their list without the continue field. Otherwise, the client may send another list request with the token received with the 410 error, the server will respond with a list starting from the next key, but from the latest snapshot, which is inconsistent from the previous list results - objects that are created, modified, or deleted after the first list request will be included in the response, as long as their keys are after the \&quot;next key\&quot;.  This field is not supported when watch is true. Clients may start a watch from the last resourceVersion value returned by the server and not miss any modifications.
    :type _continue: str
    :param dry_run: When present, indicates that modifications should not be persisted. An invalid or unrecognized dryRun directive will result in an error response and no further processing of the request. Valid values are: - All: all dry run stages will be processed
    :type dry_run: str
    :param field_selector: A selector to restrict the list of returned objects by their fields. Defaults to everything.
    :type field_selector: str
    :param grace_period_seconds: The duration in seconds before the object should be deleted. Value must be non-negative integer. The value zero indicates delete immediately. If this value is nil, the default grace period for the specified type will be used. Defaults to a per object value if not specified. zero means delete immediately.
    :type grace_period_seconds: int
    :param label_selector: A selector to restrict the list of returned objects by their labels. Defaults to everything.
    :type label_selector: str
    :param limit: limit is a maximum number of responses to return for a list call. If more items exist, the server will set the &#x60;continue&#x60; field on the list metadata to a value that can be used with the same initial query to retrieve the next set of results. Setting a limit may return fewer than the requested amount of items (up to zero items) in the event all requested objects are filtered out and clients should only use the presence of the continue field to determine whether more results are available. Servers may choose not to support the limit argument and will return all of the available results. If limit is specified and the continue field is empty, clients may assume that no more results are available. This field is not supported if watch is true.  The server guarantees that the objects returned when using continue will be identical to issuing a single list call without a limit - that is, no objects created, modified, or deleted after the first request is issued will be included in any subsequent continued requests. This is sometimes referred to as a consistent snapshot, and ensures that a client that is using limit to receive smaller chunks of a very large result can ensure they see all possible objects. If objects are updated during a chunked list the version of the object that was present at the time the first list result was calculated is returned.
    :type limit: int
    :param orphan_dependents: Deprecated: please use the PropagationPolicy, this field will be deprecated in 1.7. Should the dependent objects be orphaned. If true/false, the \&quot;orphan\&quot; finalizer will be added to/removed from the object&#x27;s finalizers list. Either this field or PropagationPolicy may be set, but not both.
    :type orphan_dependents: bool
    :param propagation_policy: Whether and how garbage collection will be performed. Either this field or OrphanDependents may be set, but not both. The default policy is decided by the existing finalizer set in the metadata.finalizers and the resource-specific default policy. Acceptable values are: &#x27;Orphan&#x27; - orphan the dependents; &#x27;Background&#x27; - allow the garbage collector to delete the dependents in the background; &#x27;Foreground&#x27; - a cascading policy that deletes all dependents in the foreground.
    :type propagation_policy: str
    :param resource_version: resourceVersion sets a constraint on what resource versions a request may be served from. See https://kubernetes.io/docs/reference/using-api/api-concepts/#resource-versions for details.  Defaults to unset
    :type resource_version: str
    :param resource_version_match: resourceVersionMatch determines how resourceVersion is applied to list calls. It is highly recommended that resourceVersionMatch be set for list calls where resourceVersion is set See https://kubernetes.io/docs/reference/using-api/api-concepts/#resource-versions for details.  Defaults to unset
    :type resource_version_match: str
    :param timeout_seconds: Timeout for the list/watch call. This limits the duration of the call, regardless of any activity or inactivity.
    :type timeout_seconds: int

    :rtype: IoK8sApimachineryPkgApisMetaV1Status
    """
    return 'do some magic!'


def delete_core_v1alpha1_collection_physical_node(pretty=None, _continue=None, dry_run=None, field_selector=None, grace_period_seconds=None, label_selector=None, limit=None, orphan_dependents=None, propagation_policy=None, resource_version=None, resource_version_match=None, timeout_seconds=None):  # noqa: E501
    """delete_core_v1alpha1_collection_physical_node

    delete collection of PhysicalNode # noqa: E501

    :param pretty: If &#x27;true&#x27;, then the output is pretty printed.
    :type pretty: str
    :param _continue: The continue option should be set when retrieving more results from the server. Since this value is server defined, clients may only use the continue value from a previous query result with identical query parameters (except for the value of continue) and the server may reject a continue value it does not recognize. If the specified continue value is no longer valid whether due to expiration (generally five to fifteen minutes) or a configuration change on the server, the server will respond with a 410 ResourceExpired error together with a continue token. If the client needs a consistent list, it must restart their list without the continue field. Otherwise, the client may send another list request with the token received with the 410 error, the server will respond with a list starting from the next key, but from the latest snapshot, which is inconsistent from the previous list results - objects that are created, modified, or deleted after the first list request will be included in the response, as long as their keys are after the \&quot;next key\&quot;.  This field is not supported when watch is true. Clients may start a watch from the last resourceVersion value returned by the server and not miss any modifications.
    :type _continue: str
    :param dry_run: When present, indicates that modifications should not be persisted. An invalid or unrecognized dryRun directive will result in an error response and no further processing of the request. Valid values are: - All: all dry run stages will be processed
    :type dry_run: str
    :param field_selector: A selector to restrict the list of returned objects by their fields. Defaults to everything.
    :type field_selector: str
    :param grace_period_seconds: The duration in seconds before the object should be deleted. Value must be non-negative integer. The value zero indicates delete immediately. If this value is nil, the default grace period for the specified type will be used. Defaults to a per object value if not specified. zero means delete immediately.
    :type grace_period_seconds: int
    :param label_selector: A selector to restrict the list of returned objects by their labels. Defaults to everything.
    :type label_selector: str
    :param limit: limit is a maximum number of responses to return for a list call. If more items exist, the server will set the &#x60;continue&#x60; field on the list metadata to a value that can be used with the same initial query to retrieve the next set of results. Setting a limit may return fewer than the requested amount of items (up to zero items) in the event all requested objects are filtered out and clients should only use the presence of the continue field to determine whether more results are available. Servers may choose not to support the limit argument and will return all of the available results. If limit is specified and the continue field is empty, clients may assume that no more results are available. This field is not supported if watch is true.  The server guarantees that the objects returned when using continue will be identical to issuing a single list call without a limit - that is, no objects created, modified, or deleted after the first request is issued will be included in any subsequent continued requests. This is sometimes referred to as a consistent snapshot, and ensures that a client that is using limit to receive smaller chunks of a very large result can ensure they see all possible objects. If objects are updated during a chunked list the version of the object that was present at the time the first list result was calculated is returned.
    :type limit: int
    :param orphan_dependents: Deprecated: please use the PropagationPolicy, this field will be deprecated in 1.7. Should the dependent objects be orphaned. If true/false, the \&quot;orphan\&quot; finalizer will be added to/removed from the object&#x27;s finalizers list. Either this field or PropagationPolicy may be set, but not both.
    :type orphan_dependents: bool
    :param propagation_policy: Whether and how garbage collection will be performed. Either this field or OrphanDependents may be set, but not both. The default policy is decided by the existing finalizer set in the metadata.finalizers and the resource-specific default policy. Acceptable values are: &#x27;Orphan&#x27; - orphan the dependents; &#x27;Background&#x27; - allow the garbage collector to delete the dependents in the background; &#x27;Foreground&#x27; - a cascading policy that deletes all dependents in the foreground.
    :type propagation_policy: str
    :param resource_version: resourceVersion sets a constraint on what resource versions a request may be served from. See https://kubernetes.io/docs/reference/using-api/api-concepts/#resource-versions for details.  Defaults to unset
    :type resource_version: str
    :param resource_version_match: resourceVersionMatch determines how resourceVersion is applied to list calls. It is highly recommended that resourceVersionMatch be set for list calls where resourceVersion is set See https://kubernetes.io/docs/reference/using-api/api-concepts/#resource-versions for details.  Defaults to unset
    :type resource_version_match: str
    :param timeout_seconds: Timeout for the list/watch call. This limits the duration of the call, regardless of any activity or inactivity.
    :type timeout_seconds: int

    :rtype: IoK8sApimachineryPkgApisMetaV1Status
    """
    return 'do some magic!'


def delete_core_v1alpha1_node(tenant_id, node_name, pretty=None, dry_run=None, grace_period_seconds=None, orphan_dependents=None, propagation_policy=None):  # noqa: E501
    """delete_core_v1alpha1_node

    delete a Node # noqa: E501

    :param tenant_id: name of the tenant
    :type tenant_id: str
    :param node_name: name of the Node
    :type node_name: str
    :param pretty: If &#x27;true&#x27;, then the output is pretty printed.
    :type pretty: str
    :param dry_run: When present, indicates that modifications should not be persisted. An invalid or unrecognized dryRun directive will result in an error response and no further processing of the request. Valid values are: - All: all dry run stages will be processed
    :type dry_run: str
    :param grace_period_seconds: The duration in seconds before the object should be deleted. Value must be non-negative integer. The value zero indicates delete immediately. If this value is nil, the default grace period for the specified type will be used. Defaults to a per object value if not specified. zero means delete immediately.
    :type grace_period_seconds: int
    :param orphan_dependents: Deprecated: please use the PropagationPolicy, this field will be deprecated in 1.7. Should the dependent objects be orphaned. If true/false, the \&quot;orphan\&quot; finalizer will be added to/removed from the object&#x27;s finalizers list. Either this field or PropagationPolicy may be set, but not both.
    :type orphan_dependents: bool
    :param propagation_policy: Whether and how garbage collection will be performed. Either this field or OrphanDependents may be set, but not both. The default policy is decided by the existing finalizer set in the metadata.finalizers and the resource-specific default policy. Acceptable values are: &#x27;Orphan&#x27; - orphan the dependents; &#x27;Background&#x27; - allow the garbage collector to delete the dependents in the background; &#x27;Foreground&#x27; - a cascading policy that deletes all dependents in the foreground.
    :type propagation_policy: str

    :rtype: IoK8sApimachineryPkgApisMetaV1Status
    """

    resOK,accountType,accountID = getAccountType()

    if not resOK:
        return (accountType,401)

    if accountType == 'tenant' and accountID != tenant_id:
        print ('Unauthorized tenant:'+accountID)
        return ('Unauthorized tenant:'+accountID,401)

    kubecon = swagger_server.kubernetesAPI.get_kubecon()

    try:
        k8sStatus = kubecon.delete_namespaced_pod(name = node_name, namespace = tenant_id)

    except swagger_server.kubernetesAPI.client.rest.ApiException as e:
        #print("Exception when calling CoreV1Api->read_namespace: %s\n" % e)
        #statusK8s = json.loads(e.body,object_hook=swagger_server.kubernetesAPI.client.V1Status);
        if e.status == 404:
           statusReturn = IoK8sApimachineryPkgApisMetaV1Status(kind="Status",api_version="v1alpha1",metadata={},status="Failure",message="node \""+node_name+"\" not found",reason="NotFound",details=IoK8sApimachineryPkgApisMetaV1StatusDetails(name=tenant_id,kind="node"),code=404)
           return (statusReturn,404)

        print("Exception when calling CoreV1Api->create_namespace: %s\n" % e)
        statusReturn = IoK8sApimachineryPkgApisMetaV1Status(kind="Status",api_version="v1alpha1",metadata={},status="Failure",message="node \""+node_name+"\" error",reason="NotFound",details=IoK8sApimachineryPkgApisMetaV1StatusDetails(name=tenant_id,kind="node"),code=409)
        return (statusReturn,404)

    statusReturn = IoK8sApimachineryPkgApisMetaV1Status(kind="Status",api_version="v1alpha1",metadata={},status="Success",message="node \""+node_name+"\" deleted",reason="OK",details=IoK8sApimachineryPkgApisMetaV1StatusDetails(name=tenant_id,kind="node"),code=200)
    return statusReturn

def delete_core_v1alpha1_physical_node(node_name, pretty=None, dry_run=None, grace_period_seconds=None, orphan_dependents=None, propagation_policy=None):  # noqa: E501
    """delete_core_v1alpha1_physical_node

    delete a PhysicalNode # noqa: E501

    :param node_name: name of the PhysicalNode
    :type node_name: str
    :param pretty: If &#x27;true&#x27;, then the output is pretty printed.
    :type pretty: str
    :param dry_run: When present, indicates that modifications should not be persisted. An invalid or unrecognized dryRun directive will result in an error response and no further processing of the request. Valid values are: - All: all dry run stages will be processed
    :type dry_run: str
    :param grace_period_seconds: The duration in seconds before the object should be deleted. Value must be non-negative integer. The value zero indicates delete immediately. If this value is nil, the default grace period for the specified type will be used. Defaults to a per object value if not specified. zero means delete immediately.
    :type grace_period_seconds: int
    :param orphan_dependents: Deprecated: please use the PropagationPolicy, this field will be deprecated in 1.7. Should the dependent objects be orphaned. If true/false, the \&quot;orphan\&quot; finalizer will be added to/removed from the object&#x27;s finalizers list. Either this field or PropagationPolicy may be set, but not both.
    :type orphan_dependents: bool
    :param propagation_policy: Whether and how garbage collection will be performed. Either this field or OrphanDependents may be set, but not both. The default policy is decided by the existing finalizer set in the metadata.finalizers and the resource-specific default policy. Acceptable values are: &#x27;Orphan&#x27; - orphan the dependents; &#x27;Background&#x27; - allow the garbage collector to delete the dependents in the background; &#x27;Foreground&#x27; - a cascading policy that deletes all dependents in the foreground.
    :type propagation_policy: str

    :rtype: IoK8sApimachineryPkgApisMetaV1Status
    """

    resOK,accountType,accountID = getAccountType()

    if not resOK:
        return (accountType,401)

    if accountType != 'broker':
        print('Unauthorized accountRole:'+accountType)
        return ('Unauthorized accountRole:'+accountType,401)

    kubecon = swagger_server.kubernetesAPI.get_kubecon()

    try:
        nodeFound = kubecon.read_node(name = node_name)
    except swagger_server.kubernetesAPI.client.rest.ApiException as e:
        statusReturn = IoK8sApimachineryPkgApisMetaV1Status(kind="Status",api_version="v1alpha1",metadata={},status="Failure",message="physicalNode \""+node_name+"\" not found",reason="NotFound",details=IoK8sApimachineryPkgApisMetaV1StatusDetails(name=node_name,kind="physicalNode"),code=404)
        return (statusReturn,404)

    #print("node:",nodeFound)
    #print("type(i.metadata.labels):",type(i.metadata.labels))
    if not isinstance(nodeFound.metadata.labels,dict):
       statusReturn = IoK8sApimachineryPkgApisMetaV1Status(kind="Status",api_version="v1alpha1",metadata={},status="Failure",message="physical node \""+node_name+"\" not enabled",reason="NotFound",details=IoK8sApimachineryPkgApisMetaV1StatusDetails(name=node_name,kind="physicalNode"),code=404)
       return (statusReturn,404)

    if not 'physicalname' in nodeFound.metadata.labels:
       statusReturn = IoK8sApimachineryPkgApisMetaV1Status(kind="Status",api_version="v1alpha1",metadata={},status="Failure",message="physical node \""+node_name+"\" not enabled",reason="NotFound",details=IoK8sApimachineryPkgApisMetaV1StatusDetails(name=node_name,kind="physicalNode"),code=404)
       return (statusReturn,404)

    # Node exists but the label does not so create ito
    nodeNew = swagger_server.kubernetesAPI.client.V1Node() 

    nodeNew  = {
              "metadata": {
                  "labels": {
                     "physicalname": None
                  }
               }
            }

    try:
        k8sNode = kubecon.patch_node(name = node_name, body = nodeNew)
    except swagger_server.kubernetesAPI.client.rest.ApiException as e:
        #print("Exception when calling CoreV1Api->read_namespace: %s\n" % e)
        #statusK8s = json.loads(e.body,object_hook=swagger_server.kubernetesAPI.client.V1Status);
        if e.status == 404:
           statusReturn = IoK8sApimachineryPkgApisMetaV1Status(kind="Status",api_version="v1alpha1",metadata={},status="Failure",message="physical node \""+node_name+"\" not found",reason="NotFound",details=IoK8sApimachineryPkgApisMetaV1StatusDetails(name=node_name,kind="physicalNode"),code=404)
           return (statusReturn,404)

    try:
        namespacesFound = kubecon.list_namespace(watch=False,label_selector="tenantName")
    except swagger_server.kubernetesAPI.client.rest.ApiException as e:
        #print("Exception when calling CoreV1Api->read_namespace: %s\n" % e)
        #statusK8s = json.loads(e.body,object_hook=swagger_server.kubernetesAPI.client.V1Status);
        if e.status == 404:
           statusReturn = IoK8sApimachineryPkgApisMetaV1Status(kind="Status",api_version="v1alpha1",metadata={},status="Failure",message="physical node \""+node_name+"\" not found",reason="NotFound",details=IoK8sApimachineryPkgApisMetaV1StatusDetails(name=node_name,kind="physicalNode"),code=404)
           return (statusReturn,404)

        print("Exception when calling CoreV1Api->create_namespace: %s\n" % e)
        statusReturn = IoK8sApimachineryPkgApisMetaV1Status(kind="Status",api_version="v1alpha1",metadata={},status="Failure",message="physical node \""+node_name+"\" error",reason="InternalError",details=IoK8sApimachineryPkgApisMetaV1StatusDetails(name=node_name,kind="physicalNode"),code=409)
        return (statusReturn,409)

    tenantsList = ArmBrokerageApiCoreV1alpha1TenantList(api_version="v1alpha1",kind="Tenants",items=[])  # noqa: E501

    for i in namespacesFound.items:
       #print("namespace:",i)
       #print("type(i.metadata.labels):",type(i.metadata.labels))
       try:
          podList = kubecon.list_namespaced_pod(i.metadata.name, field_selector="hostname="+node_name)

          for podDelete in podList.items:
             #print("pod:",podDelete)

             kubecon.delete_namespaced_pod(i.metadata.name, podDelete.metadata.name)
       except swagger_server.kubernetesAPI.client.rest.ApiException as e:
          print("No pods found")

    statusReturn = IoK8sApimachineryPkgApisMetaV1Status(kind="Status",api_version="v1alpha1",metadata={},status="Success",message="physicalnode \""+node_name+"\" deleted",reason="OK",details=IoK8sApimachineryPkgApisMetaV1StatusDetails(name=node_name,kind="physicalNode"),code=200)
    return statusReturn

def delete_core_v1alpha1_tenant(tenant_id, pretty=None, dry_run=None, grace_period_seconds=None, orphan_dependents=None, propagation_policy=None):  # noqa: E501
    """delete_core_v1alpha1_tenant

    delete a Tenant # noqa: E501

    :param tenant_id: name of the Tenant
    :type tenant_id: str
    :param pretty: If &#x27;true&#x27;, then the output is pretty printed.
    :type pretty: str
    :param dry_run: When present, indicates that modifications should not be persisted. An invalid or unrecognized dryRun directive will result in an error response and no further processing of the request. Valid values are: - All: all dry run stages will be processed
    :type dry_run: str
    :param grace_period_seconds: The duration in seconds before the object should be deleted. Value must be non-negative integer. The value zero indicates delete immediately. If this value is nil, the default grace period for the specified type will be used. Defaults to a per object value if not specified. zero means delete immediately.
    :type grace_period_seconds: int
    :param orphan_dependents: Deprecated: please use the PropagationPolicy, this field will be deprecated in 1.7. Should the dependent objects be orphaned. If true/false, the \&quot;orphan\&quot; finalizer will be added to/removed from the object&#x27;s finalizers list. Either this field or PropagationPolicy may be set, but not both.
    :type orphan_dependents: bool
    :param propagation_policy: Whether and how garbage collection will be performed. Either this field or OrphanDependents may be set, but not both. The default policy is decided by the existing finalizer set in the metadata.finalizers and the resource-specific default policy. Acceptable values are: &#x27;Orphan&#x27; - orphan the dependents; &#x27;Background&#x27; - allow the garbage collector to delete the dependents in the background; &#x27;Foreground&#x27; - a cascading policy that deletes all dependents in the foreground.
    :type propagation_policy: str

    :rtype: ArmBrokerageApiCoreV1alpha1Tenant
    """
    resOK,accountType,accountID = getAccountType()

    if not resOK:
        return (accountType,401)

    if accountType != 'broker':
        print('Unauthorized accountRole:'+accountType)
        return ('Unauthorized accountRole:'+accountType,401)

    kubecon = swagger_server.kubernetesAPI.get_kubecon()

    try:
        namespaceFound = kubecon.read_namespace(tenant_id)

    except swagger_server.kubernetesAPI.client.rest.ApiException as e:
        if e.status == 404:
           statusReturn = IoK8sApimachineryPkgApisMetaV1Status(kind="Status",api_version="v1alpha1",metadata={},status="Failure",message="tenant \""+tenant_id+"\" not found",reason="NotFound",details=IoK8sApimachineryPkgApisMetaV1StatusDetails(name=tenant_id,kind="tenant"),code=404)
           return (statusReturn,404)

        print("Exception when calling CoreV1Api->create_namespace: %s\n" % e)
        statusReturn = IoK8sApimachineryPkgApisMetaV1Status(kind="Status",api_version="v1alpha1",metadata={},status="Failure",message="tenant \""+tenant_id+"\" error",reason="NotFound",details=IoK8sApimachineryPkgApisMetaV1StatusDetails(name=tenant_id,kind="tenant"),code=409)
        return (statusReturn,404)

    #print("namespace:", namespaceFound)
    #print("type(namespaceFound.metadata.labels):",type(namespaceFound.metadata.labels))
    if isinstance(namespaceFound.metadata.labels,dict):
       if 'tenantName' in namespaceFound.metadata.labels:
          namespaceStatus = kubecon.delete_namespace(tenant_id)
          return ArmBrokerageApiCoreV1alpha1Tenant(id = namespaceFound.metadata.name, name =  decode64k8s(namespaceFound.metadata.labels['tenantName']), credential = credentialFromAnnotations(namespaceFound.metadata.annotations))
    statusReturn = IoK8sApimachineryPkgApisMetaV1Status(kind="Status",api_version="v1alpha1",metadata={},status="Failure",message="tenant \""+tenant_id+"\" not found",reason="NotFound",details=IoK8sApimachineryPkgApisMetaV1StatusDetails(name=tenant_id,kind="tenant"),code=404)
    return (statusReturn,404)

def get_core_v1_api_resources():  # noqa: E501
    """get_core_v1_api_resources

    get available resources # noqa: E501


    :rtype: IoK8sApimachineryPkgApisMetaV1APIResourceList
    """
    return IoK8sApimachineryPkgApisMetaV1APIResourceList(api_version="v1alpha1",group_version="", kind="APIResource", resources=[ \
            IoK8sApimachineryPkgApisMetaV1APIResource(name="tenants",singular_name="tenant",namespaced=False,kind="Tenant",verbs=["get","list","delete","deletecollection"]), \
            IoK8sApimachineryPkgApisMetaV1APIResource(name="nodes",singular_name="node",namespaced=False,kind="Tenant/Node",verbs=["get","list","delete","deletecollection"]), \
            IoK8sApimachineryPkgApisMetaV1APIResource(name="physicalNodes",singular_name="physicalNode",namespaced=False,kind="PhysicalNode",verbs=["get","list","delete","deletecollection"])])  # noqa: E501

def list_core_v1alpha1_node(tenant_id, pretty=None, allow_watch_bookmarks=None, _continue=None, field_selector=None, label_selector=None, limit=None, resource_version=None, resource_version_match=None, timeout_seconds=None):  # noqa: E501
    """list_core_v1alpha1_node

    list or watch objects of kind Node # noqa: E501

    :param tenant_id: name of the Tenant
    :type tenant_id: str
    :param pretty: If &#x27;true&#x27;, then the output is pretty printed.
    :type pretty: str
    :param allow_watch_bookmarks: allowWatchBookmarks requests watch events with type \&quot;BOOKMARK\&quot;. Servers that do not implement bookmarks may ignore this flag and bookmarks are sent at the server&#x27;s discretion. Clients should not assume bookmarks are returned at any specific interval, nor may they assume the server will send any BOOKMARK event during a session. If this is not a watch, this field is ignored. If the feature gate WatchBookmarks is not enabled in apiserver, this field is ignored.
    :type allow_watch_bookmarks: bool
    :param _continue: The continue option should be set when retrieving more results from the server. Since this value is server defined, clients may only use the continue value from a previous query result with identical query parameters (except for the value of continue) and the server may reject a continue value it does not recognize. If the specified continue value is no longer valid whether due to expiration (generally five to fifteen minutes) or a configuration change on the server, the server will respond with a 410 ResourceExpired error together with a continue token. If the client needs a consistent list, it must restart their list without the continue field. Otherwise, the client may send another list request with the token received with the 410 error, the server will respond with a list starting from the next key, but from the latest snapshot, which is inconsistent from the previous list results - objects that are created, modified, or deleted after the first list request will be included in the response, as long as their keys are after the \&quot;next key\&quot;.  This field is not supported when watch is true. Clients may start a watch from the last resourceVersion value returned by the server and not miss any modifications.
    :type _continue: str
    :param field_selector: A selector to restrict the list of returned objects by their fields. Defaults to everything.
    :type field_selector: str
    :param label_selector: A selector to restrict the list of returned objects by their labels. Defaults to everything.
    :type label_selector: str
    :param limit: limit is a maximum number of responses to return for a list call. If more items exist, the server will set the &#x60;continue&#x60; field on the list metadata to a value that can be used with the same initial query to retrieve the next set of results. Setting a limit may return fewer than the requested amount of items (up to zero items) in the event all requested objects are filtered out and clients should only use the presence of the continue field to determine whether more results are available. Servers may choose not to support the limit argument and will return all of the available results. If limit is specified and the continue field is empty, clients may assume that no more results are available. This field is not supported if watch is true.  The server guarantees that the objects returned when using continue will be identical to issuing a single list call without a limit - that is, no objects created, modified, or deleted after the first request is issued will be included in any subsequent continued requests. This is sometimes referred to as a consistent snapshot, and ensures that a client that is using limit to receive smaller chunks of a very large result can ensure they see all possible objects. If objects are updated during a chunked list the version of the object that was present at the time the first list result was calculated is returned.
    :type limit: int
    :param resource_version: resourceVersion sets a constraint on what resource versions a request may be served from. See https://kubernetes.io/docs/reference/using-api/api-concepts/#resource-versions for details.  Defaults to unset
    :type resource_version: str
    :param resource_version_match: resourceVersionMatch determines how resourceVersion is applied to list calls. It is highly recommended that resourceVersionMatch be set for list calls where resourceVersion is set See https://kubernetes.io/docs/reference/using-api/api-concepts/#resource-versions for details.  Defaults to unset
    :type resource_version_match: str
    :param timeout_seconds: Timeout for the list/watch call. This limits the duration of the call, regardless of any activity or inactivity.
    :type timeout_seconds: int

    :rtype: ArmBrokerageApiCoreV1alpha1NodeList
    """

    resOK,accountType,accountID = getAccountType()

    if not resOK:
        return (accountType,401)

    if accountType == 'tenant' and accountID != tenant_id:
        print ('Unauthorized tenant:'+accountID)
        return ('Unauthorized tenant:'+accountID,401)

    kubecon = swagger_server.kubernetesAPI.get_kubecon()

    try:
        podsFound = kubecon.list_namespaced_pod(namespace=tenant_id, watch=False, label_selector="physicalname")

    except swagger_server.kubernetesAPI.client.rest.ApiException as e:
        #print("Exception when calling CoreV1Api->read_namespace: %s\n" % e)
        #statusK8s = json.loads(e.body,object_hook=swagger_server.kubernetesAPI.client.V1Status);
        if e.status == 404:
           statusReturn = IoK8sApimachineryPkgApisMetaV1Status(kind="Status",api_version="v1alpha1",metadata={},status="Failure",message="node \""+node_name+"\" not found",reason="NotFound",details=IoK8sApimachineryPkgApisMetaV1StatusDetails(name=tenant_id,kind="node"),code=404)
           return (statusReturn,404)

        print("Exception when calling CoreV1Api->create_namespace: %s\n" % e)
        statusReturn = IoK8sApimachineryPkgApisMetaV1Status(kind="Status",api_version="v1alpha1",metadata={},status="Failure",message="node \""+node_name+"\" error",reason="NotFound",details=IoK8sApimachineryPkgApisMetaV1StatusDetails(name=tenant_id,kind="node"),code=409)
        return (statusReturn,404)

    nodesList = ArmBrokerageApiCoreV1alpha1NodeList(api_version="v1alpha1",kind="Nodes",items=[])  # noqa: E501

    for i in podsFound.items:
       #print("pod:",i)
       #print("type(i.metadata.labels):",type(i.metadata.labels))
       if isinstance(i.metadata.labels,dict):
          if 'physicalname' in i.metadata.labels:
             nodesList.items.append(ArmBrokerageApiCoreV1alpha1Node(
                                        id = i.metadata.name, 
                                        name = i.metadata.labels['physicalname'], 
                                        physical_node = i.spec.node_name, 
                                        credential = credentialFromAnnotations(i.metadata.annotations),
                                        resources = reourcesfromV1Resources(i.spec.containers[0].resources)))
          else:
             nodesList.items.append(ArmBrokerageApiCoreV1alpha1Node(id = i.metadata.name, name = ''))
       else: 
          nodesList.items.append(ArmBrokerageApiCoreV1alpha1Node(id = i.metadata.name, name = ''))

    return nodesList

def list_core_v1alpha1_physical_node(pretty=None, allow_watch_bookmarks=None, _continue=None, field_selector=None, label_selector=None, limit=None, resource_version=None, resource_version_match=None, timeout_seconds=None):  # noqa: E501
    """list_core_v1alpha1_physical_node

    list or watch objects of kind PhysicalNode # noqa: E501

    :param pretty: If &#x27;true&#x27;, then the output is pretty printed.
    :type pretty: str
    :param allow_watch_bookmarks: allowWatchBookmarks requests watch events with type \&quot;BOOKMARK\&quot;. Servers that do not implement bookmarks may ignore this flag and bookmarks are sent at the server&#x27;s discretion. Clients should not assume bookmarks are returned at any specific interval, nor may they assume the server will send any BOOKMARK event during a session. If this is not a watch, this field is ignored. If the feature gate WatchBookmarks is not enabled in apiserver, this field is ignored.
    :type allow_watch_bookmarks: bool
    :param _continue: The continue option should be set when retrieving more results from the server. Since this value is server defined, clients may only use the continue value from a previous query result with identical query parameters (except for the value of continue) and the server may reject a continue value it does not recognize. If the specified continue value is no longer valid whether due to expiration (generally five to fifteen minutes) or a configuration change on the server, the server will respond with a 410 ResourceExpired error together with a continue token. If the client needs a consistent list, it must restart their list without the continue field. Otherwise, the client may send another list request with the token received with the 410 error, the server will respond with a list starting from the next key, but from the latest snapshot, which is inconsistent from the previous list results - objects that are created, modified, or deleted after the first list request will be included in the response, as long as their keys are after the \&quot;next key\&quot;.  This field is not supported when watch is true. Clients may start a watch from the last resourceVersion value returned by the server and not miss any modifications.
    :type _continue: str
    :param field_selector: A selector to restrict the list of returned objects by their fields. Defaults to everything.
    :type field_selector: str
    :param label_selector: A selector to restrict the list of returned objects by their labels. Defaults to everything.
    :type label_selector: str
    :param limit: limit is a maximum number of responses to return for a list call. If more items exist, the server will set the &#x60;continue&#x60; field on the list metadata to a value that can be used with the same initial query to retrieve the next set of results. Setting a limit may return fewer than the requested amount of items (up to zero items) in the event all requested objects are filtered out and clients should only use the presence of the continue field to determine whether more results are available. Servers may choose not to support the limit argument and will return all of the available results. If limit is specified and the continue field is empty, clients may assume that no more results are available. This field is not supported if watch is true.  The server guarantees that the objects returned when using continue will be identical to issuing a single list call without a limit - that is, no objects created, modified, or deleted after the first request is issued will be included in any subsequent continued requests. This is sometimes referred to as a consistent snapshot, and ensures that a client that is using limit to receive smaller chunks of a very large result can ensure they see all possible objects. If objects are updated during a chunked list the version of the object that was present at the time the first list result was calculated is returned.
    :type limit: int
    :param resource_version: resourceVersion sets a constraint on what resource versions a request may be served from. See https://kubernetes.io/docs/reference/using-api/api-concepts/#resource-versions for details.  Defaults to unset
    :type resource_version: str
    :param resource_version_match: resourceVersionMatch determines how resourceVersion is applied to list calls. It is highly recommended that resourceVersionMatch be set for list calls where resourceVersion is set See https://kubernetes.io/docs/reference/using-api/api-concepts/#resource-versions for details.  Defaults to unset
    :type resource_version_match: str
    :param timeout_seconds: Timeout for the list/watch call. This limits the duration of the call, regardless of any activity or inactivity.
    :type timeout_seconds: int

    :rtype: ArmBrokerageApiCoreV1alpha1PhysicalNodeList
    """
    resOK,accountType,accountID = getAccountType()

    if not resOK:
        return (accountType,401)

    if accountType != 'broker':
        print('Unauthorized accountRole:'+accountType)
        return ('Unauthorized accountRole:'+accountType,401)

    kubecon = swagger_server.kubernetesAPI.get_kubecon()

    nodesFound = kubecon.list_node(watch=False,label_selector="physicalname")

    physicalNodeList = ArmBrokerageApiCoreV1alpha1PhysicalNodeList(api_version="v1alpha1",kind="PhysicalNodes",items=[])  # noqa: E501

    for i in nodesFound.items:
       #print("node:",i)
       #print("type(i.metadata.labels):",type(i.metadata.labels))
       if isinstance(i.metadata.labels,dict):
          if 'physicalname' in i.metadata.labels:
             physicalNodeList.items.append(ArmBrokerageApiCoreV1alpha1PhysicalNode(id = i.metadata.name, name = decode64k8s(i.metadata.labels['physicalname'])))
          else:
             physicalNode.items.append(ArmBrokerageApiCoreV1alpha1Tenant(id = i.metadata.name))
       else: 
          physicalNode.items.append(ArmBrokerageApiCoreV1alpha1Tenant(id = i.metadata.name))

    return physicalNodeList

def list_core_v1alpha1_tenants(pretty=None, allow_watch_bookmarks=None, _continue=None, field_selector=None, label_selector=None, limit=None, resource_version=None, resource_version_match=None, timeout_seconds=None):  # noqa: E501
    """list_core_v1alpha1_tenants

    list or watch objects of kind Tenants # noqa: E501

    :param pretty: If &#x27;true&#x27;, then the output is pretty printed.
    :type pretty: str
    :param allow_watch_bookmarks: allowWatchBookmarks requests watch events with type \&quot;BOOKMARK\&quot;. Servers that do not implement bookmarks may ignore this flag and bookmarks are sent at the server&#x27;s discretion. Clients should not assume bookmarks are returned at any specific interval, nor may they assume the server will send any BOOKMARK event during a session. If this is not a watch, this field is ignored. If the feature gate WatchBookmarks is not enabled in apiserver, this field is ignored.
    :type allow_watch_bookmarks: bool
    :param _continue: The continue option should be set when retrieving more results from the server. Since this value is server defined, clients may only use the continue value from a previous query result with identical query parameters (except for the value of continue) and the server may reject a continue value it does not recognize. If the specified continue value is no longer valid whether due to expiration (generally five to fifteen minutes) or a configuration change on the server, the server will respond with a 410 ResourceExpired error together with a continue token. If the client needs a consistent list, it must restart their list without the continue field. Otherwise, the client may send another list request with the token received with the 410 error, the server will respond with a list starting from the next key, but from the latest snapshot, which is inconsistent from the previous list results - objects that are created, modified, or deleted after the first list request will be included in the response, as long as their keys are after the \&quot;next key\&quot;.  This field is not supported when watch is true. Clients may start a watch from the last resourceVersion value returned by the server and not miss any modifications.
    :type _continue: str
    :param field_selector: A selector to restrict the list of returned objects by their fields. Defaults to everything.
    :type field_selector: str
    :param label_selector: A selector to restrict the list of returned objects by their labels. Defaults to everything.
    :type label_selector: str
    :param limit: limit is a maximum number of responses to return for a list call. If more items exist, the server will set the &#x60;continue&#x60; field on the list metadata to a value that can be used with the same initial query to retrieve the next set of results. Setting a limit may return fewer than the requested amount of items (up to zero items) in the event all requested objects are filtered out and clients should only use the presence of the continue field to determine whether more results are available. Servers may choose not to support the limit argument and will return all of the available results. If limit is specified and the continue field is empty, clients may assume that no more results are available. This field is not supported if watch is true.  The server guarantees that the objects returned when using continue will be identical to issuing a single list call without a limit - that is, no objects created, modified, or deleted after the first request is issued will be included in any subsequent continued requests. This is sometimes referred to as a consistent snapshot, and ensures that a client that is using limit to receive smaller chunks of a very large result can ensure they see all possible objects. If objects are updated during a chunked list the version of the object that was present at the time the first list result was calculated is returned.
    :type limit: int
    :param resource_version: resourceVersion sets a constraint on what resource versions a request may be served from. See https://kubernetes.io/docs/reference/using-api/api-concepts/#resource-versions for details.  Defaults to unset
    :type resource_version: str
    :param resource_version_match: resourceVersionMatch determines how resourceVersion is applied to list calls. It is highly recommended that resourceVersionMatch be set for list calls where resourceVersion is set See https://kubernetes.io/docs/reference/using-api/api-concepts/#resource-versions for details.  Defaults to unset
    :type resource_version_match: str
    :param timeout_seconds: Timeout for the list/watch call. This limits the duration of the call, regardless of any activity or inactivity.
    :type timeout_seconds: int

    :rtype: ArmBrokerageApiCoreV1alpha1TenantList
    """

    resOK,accountType,accountID = getAccountType()

    if not resOK:
        return (accountType,401)

    if accountType != 'broker':
        print('Unauthorized accountRole:'+accountType)
        return ('Unauthorized accountRole:'+accountType,401)

    kubecon = swagger_server.kubernetesAPI.get_kubecon()

    namespacesFound = kubecon.list_namespace(watch=False,label_selector="tenantName")

    tenantsList = ArmBrokerageApiCoreV1alpha1TenantList(api_version="v1alpha1",kind="Tenants",items=[])  # noqa: E501

    for i in namespacesFound.items:
       #print("namespace:",i)
       #print("type(i.metadata.labels):",type(i.metadata.labels))
       if isinstance(i.metadata.labels,dict):
          if 'tenantName' in i.metadata.labels:
             tenantsList.items.append(ArmBrokerageApiCoreV1alpha1Tenant(
                                                id = i.metadata.name, 
                                                name = decode64k8s(i.metadata.labels['tenantName']), 
                                                credential = credentialFromAnnotations(i.metadata.annotations),
                                                resources = resourcesFromAnnotations(i.metadata.annotations)))
          else:
             tenantsList.items.append(ArmBrokerageApiCoreV1alpha1Tenant(id = i.metadata.name))
       else: 
          tenantsList.items.append(ArmBrokerageApiCoreV1alpha1Tenant(id = i.metadata.name))

    return tenantsList


def read_core_v1alpha1_node(tenant_id, node_name, pretty=None, exact=None, export=None):  # noqa: E501
    """read_core_v1alpha1_node

    read the specified Node # noqa: E501

    :param tenant_id: name of the tenant
    :type tenant_id: str
    :param node_name: name of the Node
    :type node_name: str
    :param pretty: If &#x27;true&#x27;, then the output is pretty printed.
    :type pretty: str
    :param exact: Should the export be exact.  Exact export maintains cluster-specific fields like &#x27;Namespace&#x27;. Deprecated. Planned for removal in 1.18.
    :type exact: bool
    :param export: Should this value be exported.  Export strips fields that a user can not specify. Deprecated. Planned for removal in 1.18.
    :type export: bool

    :rtype: IoK8sApimachineryPkgApisMetaV1Status
    """
    resOK,accountType,accountID = getAccountType()

    if not resOK:
        return (accountType,401)

    if accountType == 'tenant' and accountID != tenant_id:
        print ('Unauthorized tenant:'+accountID)
        return ('Unauthorized tenant:'+accountID,401)

    kubecon = swagger_server.kubernetesAPI.get_kubecon()

    try:
        podFound = kubecon.read_namespaced_pod(name = node_name, namespace = tenant_id)

    except swagger_server.kubernetesAPI.client.rest.ApiException as e:
        #print("Exception when calling CoreV1Api->read_namespace: %s\n" % e)
        #statusK8s = json.loads(e.body,object_hook=swagger_server.kubernetesAPI.client.V1Status);
        if e.status == 404:
           statusReturn = IoK8sApimachineryPkgApisMetaV1Status(kind="Status",api_version="v1alpha1",metadata={},status="Failure",message="node \""+node_name+"\" not found",reason="NotFound",details=IoK8sApimachineryPkgApisMetaV1StatusDetails(name=tenant_id,kind="node"),code=404)
           return (statusReturn,404)

        print("Exception when calling CoreV1Api->create_namespace: %s\n" % e)
        statusReturn = IoK8sApimachineryPkgApisMetaV1Status(kind="Status",api_version="v1alpha1",metadata={},status="Failure",message="node \""+node_name+"\" error",reason="NotFound",details=IoK8sApimachineryPkgApisMetaV1StatusDetails(name=tenant_id,kind="node"),code=409)
        return (statusReturn,404)

    #print("pod:",podFound)
    #print("type(podFound.metadata.labels):",type(podFound.metadata.labels))
    if isinstance(podFound.metadata.labels,dict):
       if 'physicalname' in podFound.metadata.labels:
          return ArmBrokerageApiCoreV1alpha1Node(
                        id = podFound.metadata.name, 
                        name = podFound.metadata.labels['physicalname'],
                        physical_node = podFound.spec.node_name, 
                        credential = credentialFromAnnotations(podFound.metadata.annotations),
                        resources = reourcesfromV1Resources(podFound.spec.containers[0].resources))

    statusReturn = IoK8sApimachineryPkgApisMetaV1Status(kind="Status",api_version="v1alpha1",metadata={},status="Failure",message="node \""+node_name+"\" is not enabled",reason="NotFound",details=IoK8sApimachineryPkgApisMetaV1StatusDetails(name=tenant_id,kind="node"),code=404)
    return (statusReturn,409)

def read_core_v1alpha1_node_status(tenant_id, node_name, pretty=None):  # noqa: E501
    """read_core_v1alpha1_node_status

    read status of the specified Node # noqa: E501

    :param tenant_id: name of the Tenant
    :type tenant_id: str
    :param node_name: name of the Node
    :type node_name: str
    :param pretty: If &#x27;true&#x27;, then the output is pretty printed.
    :type pretty: str

    :rtype: ArmBrokerageApiCoreV1alpha1Node
    """
    return 'do some magic!'


def read_core_v1alpha1_physical_node(node_name, pretty=None, exact=None, export=None):  # noqa: E501
    """read_core_v1alpha1_physical_node

    read the specified PhysicalNode # noqa: E501

    :param node_name: name of the PhysicalNode
    :type node_name: str
    :param pretty: If &#x27;true&#x27;, then the output is pretty printed.
    :type pretty: str
    :param exact: Should the export be exact.  Exact export maintains cluster-specific fields like &#x27;Namespace&#x27;. Deprecated. Planned for removal in 1.18.
    :type exact: bool
    :param export: Should this value be exported.  Export strips fields that a user can not specify. Deprecated. Planned for removal in 1.18.
    :type export: bool

    :rtype: ArmBrokerageApiCoreV1alpha1PhysicalNode
    """
    resOK,accountType,accountID = getAccountType()

    if not resOK:
        return (accountType,401)

    if accountType != 'broker':
        print('Unauthorized accountRole:'+accountType)
        return ('Unauthorized accountRole:'+accountType,401)

    kubecon = swagger_server.kubernetesAPI.get_kubecon()

    try:
        nodeFound = kubecon.read_node(name = node_name)
    except swagger_server.kubernetesAPI.client.rest.ApiException as e:
        statusReturn = IoK8sApimachineryPkgApisMetaV1Status(kind="Status",api_version="v1alpha1",metadata={},status="Failure",message="physicalNode \""+node_name+"\" not found",reason="NotFound",details=IoK8sApimachineryPkgApisMetaV1StatusDetails(name=node_name,kind="physicalNode"),code=404)
        return (statusReturn,404)

    #print("node:",nodeFound)
    #print("type(nodeFound.metadata.labels):",type(inodeFound.metadata.labels))
    if isinstance(nodeFound.metadata.labels,dict):
       if 'physicalname' in nodeFound.metadata.labels:
          return ArmBrokerageApiCoreV1alpha1PhysicalNode(id = nodeFound.metadata.name, name = decode64k8s(nodeFound.metadata.labels['physicalname']))

    statusReturn = IoK8sApimachineryPkgApisMetaV1Status(kind="Status",api_version="v1alpha1",metadata={},status="Failure",message="physicalNode \""+node_name+"\" is not enabled",reason="NotFound",details=IoK8sApimachineryPkgApisMetaV1StatusDetails(name=node_name,kind="physicalNode"),code=404)
    return (statusReturn,404)

def read_core_v1alpha1_physical_node_status(node_name, pretty=None):  # noqa: E501
    """read_core_v1alpha1_physical_node_status

    read status of the specified PhysicalNode # noqa: E501

    :param node_name: name of the PhysicalNode
    :type node_name: str
    :param pretty: If &#x27;true&#x27;, then the output is pretty printed.
    :type pretty: str

    :rtype: ArmBrokerageApiCoreV1alpha1PhysicalNode
    """
    return 'do some magic!'


def read_core_v1alpha1_tenant(tenant_id, pretty=None, exact=None, export=None):  # noqa: E501
    """read_core_v1alpha1_tenant

    read the specified Tenant # noqa: E501

    :param tenant_id: name of the Tenant
    :type tenant_id: str
    :param pretty: If &#x27;true&#x27;, then the output is pretty printed.
    :type pretty: str
    :param exact: Should the export be exact.  Exact export maintains cluster-specific fields like &#x27;Namespace&#x27;. Deprecated. Planned for removal in 1.18.
    :type exact: bool
    :param export: Should this value be exported.  Export strips fields that a user can not specify. Deprecated. Planned for removal in 1.18.
    :type export: bool

    :rtype: ArmBrokerageApiCoreV1alpha1Tenant
    """
    resOK,accountType,accountID = getAccountType()

    if not resOK:
        return (accountType,401)

    if accountType == 'tenant' and accountID != tenant_id:
        print ('Unauthorized tenant:'+accountID)
        return ('Unauthorized tenant:'+accountID,401)

    kubecon = swagger_server.kubernetesAPI.get_kubecon()

    try:
        namespaceFound = kubecon.read_namespace(tenant_id)

    except swagger_server.kubernetesAPI.client.rest.ApiException as e:
        #print("Exception when calling CoreV1Api->read_namespace: %s\n" % e)
        #statusK8s = json.loads(e.body,object_hook=swagger_server.kubernetesAPI.client.V1Status);
        if e.status == 404:
           statusReturn = IoK8sApimachineryPkgApisMetaV1Status(kind="Status",api_version="v1alpha1",metadata={},status="Failure",message="tenant \""+tenant_id+"\" not found",reason="NotFound",details=IoK8sApimachineryPkgApisMetaV1StatusDetails(name=tenant_id,kind="tenant"),code=404)
           return (statusReturn,404)

        print("Exception when calling CoreV1Api->create_namespace: %s\n" % e)
        statusReturn = IoK8sApimachineryPkgApisMetaV1Status(kind="Status",api_version="v1alpha1",metadata={},status="Failure",message="tenant \""+tenant_id+"\" error",reason="NotFound",details=IoK8sApimachineryPkgApisMetaV1StatusDetails(name=tenant_id,kind="tenant"),code=409)
        return (statusReturn,404)

    #print("namespace:", namespaceFound)
    #print("type(namespaceFound.metadata.labels):",type(namespaceFound.metadata.labels))
    if not isinstance(namespaceFound.metadata.labels,dict):
        statusReturn = IoK8sApimachineryPkgApisMetaV1Status(kind="Status",api_version="v1alpha1",metadata={},status="Failure",message="tenant \""+tenant_id+"\" not a tenant",reason="NotFound",details=IoK8sApimachineryPkgApisMetaV1StatusDetails(name=tenant_id,kind="tenant"),code=409)
        return (statusReturn,404)
    if not 'tenantName' in namespaceFound.metadata.labels:
        statusReturn = IoK8sApimachineryPkgApisMetaV1Status(kind="Status",api_version="v1alpha1",metadata={},status="Failure",message="tenant \""+tenant_id+"\" not a tenant",reason="NotFound",details=IoK8sApimachineryPkgApisMetaV1StatusDetails(name=tenant_id,kind="tenant"),code=409)
        return (statusReturn,404)

    return ArmBrokerageApiCoreV1alpha1Tenant(
                        id = namespaceFound.metadata.name, 
                        name = decode64k8s(namespaceFound.metadata.labels['tenantName']), 
                        credential = credentialFromAnnotations(namespaceFound.metadata.annotations),
                        resources = resourcesFromAnnotations(namespaceFound.metadata.annotations))

def replace_core_v1alpha1_node(body, tenant_id, node_name, pretty=None, dry_run=None, field_manager=None):  # noqa: E501
    """replace_core_v1alpha1_node

    replace the specified Node # noqa: E501

    :param body: 
    :type body: dict | bytes
    :param tenant_id: name of the tenant
    :type tenant_id: str
    :param node_name: name of the Node
    :type node_name: str
    :param pretty: If &#x27;true&#x27;, then the output is pretty printed.
    :type pretty: str
    :param dry_run: When present, indicates that modifications should not be persisted. An invalid or unrecognized dryRun directive will result in an error response and no further processing of the request. Valid values are: - All: all dry run stages will be processed
    :type dry_run: str
    :param field_manager: fieldManager is a name associated with the actor or entity that is making these changes. The value must be less than or 128 characters long, and only contain printable characters, as defined by https://golang.org/pkg/unicode/#IsPrint.
    :type field_manager: str

    :rtype: ArmBrokerageApiCoreV1alpha1Node
    """
    if connexion.request.is_json:
        body = ArmBrokerageApiCoreV1alpha1Node.from_dict(connexion.request.get_json())  # noqa: E501
    return 'do some magic!'


def replace_core_v1alpha1_node_status(body, tenant_id, node_name, pretty=None, dry_run=None, field_manager=None):  # noqa: E501
    """replace_core_v1alpha1_node_status

    replace status of the specified Node # noqa: E501

    :param body: 
    :type body: dict | bytes
    :param tenant_id: name of the Tenant
    :type tenant_id: str
    :param node_name: name of the Node
    :type node_name: str
    :param pretty: If &#x27;true&#x27;, then the output is pretty printed.
    :type pretty: str
    :param dry_run: When present, indicates that modifications should not be persisted. An invalid or unrecognized dryRun directive will result in an error response and no further processing of the request. Valid values are: - All: all dry run stages will be processed
    :type dry_run: str
    :param field_manager: fieldManager is a name associated with the actor or entity that is making these changes. The value must be less than or 128 characters long, and only contain printable characters, as defined by https://golang.org/pkg/unicode/#IsPrint.
    :type field_manager: str

    :rtype: ArmBrokerageApiCoreV1alpha1Node
    """
    if connexion.request.is_json:
        body = ArmBrokerageApiCoreV1alpha1Node.from_dict(connexion.request.get_json())  # noqa: E501
    return 'do some magic!'


def replace_core_v1alpha1_physical_node(body, node_name, pretty=None, dry_run=None, field_manager=None):  # noqa: E501
    """replace_core_v1alpha1_physical_node

    replace the specified PhysicalNode # noqa: E501

    :param body: 
    :type body: dict | bytes
    :param node_name: name of the PhysicalNode
    :type node_name: str
    :param pretty: If &#x27;true&#x27;, then the output is pretty printed.
    :type pretty: str
    :param dry_run: When present, indicates that modifications should not be persisted. An invalid or unrecognized dryRun directive will result in an error response and no further processing of the request. Valid values are: - All: all dry run stages will be processed
    :type dry_run: str
    :param field_manager: fieldManager is a name associated with the actor or entity that is making these changes. The value must be less than or 128 characters long, and only contain printable characters, as defined by https://golang.org/pkg/unicode/#IsPrint.
    :type field_manager: str

    :rtype: ArmBrokerageApiCoreV1alpha1PhysicalNode
    """
    if connexion.request.is_json:
        body = ArmBrokerageApiCoreV1alpha1PhysicalNode.from_dict(connexion.request.get_json())  # noqa: E501
    return 'do some magic!'


def replace_core_v1alpha1_physical_node_status(body, node_name, pretty=None, dry_run=None, field_manager=None):  # noqa: E501
    """replace_core_v1alpha1_physical_node_status

    replace status of the specified PhysicalNode # noqa: E501

    :param body: 
    :type body: dict | bytes
    :param node_name: name of the PhysicalNode
    :type node_name: str
    :param pretty: If &#x27;true&#x27;, then the output is pretty printed.
    :type pretty: str
    :param dry_run: When present, indicates that modifications should not be persisted. An invalid or unrecognized dryRun directive will result in an error response and no further processing of the request. Valid values are: - All: all dry run stages will be processed
    :type dry_run: str
    :param field_manager: fieldManager is a name associated with the actor or entity that is making these changes. The value must be less than or 128 characters long, and only contain printable characters, as defined by https://golang.org/pkg/unicode/#IsPrint.
    :type field_manager: str

    :rtype: ArmBrokerageApiCoreV1alpha1PhysicalNode
    """
    if connexion.request.is_json:
        body = ArmBrokerageApiCoreV1alpha1PhysicalNode.from_dict(connexion.request.get_json())  # noqa: E501
    return 'do some magic!'


def replace_core_v1alpha1_tenant(body, tenant_id, pretty=None, dry_run=None, field_manager=None):  # noqa: E501
    """replace_core_v1alpha1_tenant

    replace the specified Tenant # noqa: E501

    :param body: 
    :type body: dict | bytes
    :param tenant_id: name of the Tenant
    :type tenant_id: str
    :param pretty: If &#x27;true&#x27;, then the output is pretty printed.
    :type pretty: str
    :param dry_run: When present, indicates that modifications should not be persisted. An invalid or unrecognized dryRun directive will result in an error response and no further processing of the request. Valid values are: - All: all dry run stages will be processed
    :type dry_run: str
    :param field_manager: fieldManager is a name associated with the actor or entity that is making these changes. The value must be less than or 128 characters long, and only contain printable characters, as defined by https://golang.org/pkg/unicode/#IsPrint.
    :type field_manager: str

    :rtype: ArmBrokerageApiCoreV1alpha1Tenant
    """
    if connexion.request.is_json:
        body = ArmBrokerageApiCoreV1alpha1Tenant.from_dict(connexion.request.get_json())  # noqa: E501
    return 'do some magic!'
